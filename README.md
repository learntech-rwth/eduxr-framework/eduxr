# OmiLAXR

OmiLAXR (Open and modular integration of Learning Analytics in XR) is a powerful and flexible technical framework that is design to support the development of educational extended reality applications.

This package contains all needed base classes for the OmiLAXR framework like `Learner` or `Learning Environment`.

## Features
- Adapters: base classes for the [OmiLAXR Adapter System](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/getting-started/omilaxr-adapters/index.html)
- Effect: a collection of components of GameObject effects (like blinking)
- Extensions: Useful functions extending existing assemblies (see documentation)
- Helpers: Some useful components for generic situations (see documentation)
- Interaction: Classes for environment interactions.
  - Laser Pointer
  - Hand Interactions
- Path: Some useful components for path drivers
- Utils: Further useful functions

### Mandatory components
For more details how to use it look into [Prefabs](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/prefabs/index.html).

- [Learner](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/scripts/OmiLAXR.Learner.html): Singleton representing the learner.
- [LearningEnvironment](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/scripts/OmiLAXR.LearningEnvironment.html): Singleton representing the learning environment.

## Dependencies

- [QuickOutline](https://github.com/SGoerzen/QuickOutline): https://github.com/SGoerzen/QuickOutline.git

## Import package
Use the git URL `https://gitlab.com/learntech-rwth/omilaxr-ecosystem/omilaxr.git` to import the package, clone as submodule or download it and import into your Assets folder.

## Creating own component registers
...

## Defining Learning Scenario, Units, Assignments and Tasks
todo...

## Contributing
Import this package as submodule directly into your project Assets folder and do contributions. Create an own branch and suggest merge requests.

## Authors and acknowledgment
Sergej Görzen, Tobias Möhring, Sam Mattiussi, Birte Heinemann

This project is mainly derived from requirements of [RePiX VR](https://gitlab.com/learntech-rwth/repix-vr/repix-vr-app). This project was born inside during the development of this learning application.

Logo was created by Jasmin Hartano

## Project status
Under exploration of requirements and development. Here you can see later more details.
