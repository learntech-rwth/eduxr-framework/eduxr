﻿using System;
using OmiLAXR.Extensions;
using OmiLAXR.Utils;

using UnityEngine;
using UnityEngine.Serialization;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace OmiLAXR.Interaction
{
    /// <summary>
    /// This class provides base functionality for click, in and out events for laser pointer
    /// </summary>
    [AddComponentMenu("OmiLAXR / Interaction / Pointable Game Object (for Laser Pointer and Mouse)")]
    [DefaultExecutionOrder(10000)]
    public class Pointable : MonoBehaviour
    {
        public static Pointable From(Transform transform)
        {
            if (transform == null || transform.gameObject == null)
                return null;
            return transform.gameObject.GetComponent<Pointable>();
        }
        
        private Material _defaultMaterial;
        private MeshRenderer _meshRenderer;

        [Tooltip("Material the object gets if laser pointer points on it.")]
        public Material hoverMaterial;

        [Tooltip("Material the object gets if user selects the pointed object.")]
        public Material selectMaterial;

        public bool reactsOnLeftPointer = true;
        public bool reactsOnRightPointer = true;

        public bool allowSelection = true;
        public bool allowClick = true;
        public bool allowHover = true;

        public bool enableDefaultMouseInteractions = true;
        [FormerlySerializedAs("highlightOnHover"), Obsolete("Will be removed in future. Please handle hover outside this component.", false)] public bool outlineOnHover;
        
        [FormerlySerializedAs("Click")] public LaserPointerEvent onClick = new LaserPointerEvent();
        [FormerlySerializedAs("Hover")] public LaserPointerEvent onHover = new LaserPointerEvent();
        [FormerlySerializedAs("Unhover")] public LaserPointerEvent onUnhover = new LaserPointerEvent();
        [FormerlySerializedAs("Select")] public LaserPointerEvent onSelect = new LaserPointerEvent();
        [FormerlySerializedAs("Unselect")] public LaserPointerEvent onUnselect = new LaserPointerEvent();

        [HideInInspector] public LaserPointerEvent onPressed = new LaserPointerEvent();
        [HideInInspector] public LaserPointerEvent onReleased = new LaserPointerEvent();
        [HideInInspector] public LaserPointerEvent onIsHolding = new LaserPointerEvent();
        [HideInInspector] public LaserPointerEvent onBeginHolding = new LaserPointerEvent();
        [HideInInspector] public LaserPointerEvent onEndHolding = new LaserPointerEvent();

        public bool IsHover => _hoverCount > 0;
        private int _hoverCount;

        public bool IsSelected => _selectedCount > 0;
        private int _selectedCount;

        public bool HasMaterialUpdates => _meshRenderer && (hoverMaterial || selectMaterial);
        
        protected virtual void Start()
        {
            _meshRenderer = GetComponent<MeshRenderer>();

            if (_meshRenderer == null)
                _meshRenderer = GetComponentInChildren<MeshRenderer>();

            if (HasMaterialUpdates)
                _defaultMaterial = _meshRenderer.material;

            if (!Learner.Instance.IsVR && enableDefaultMouseInteractions)
            {
                EnableMouseInteractions();
            }

            Init();
        }
        
        public void Init()
        {
            _hoverCount = 0;
            _selectedCount = 0;
        }
        
        protected virtual void OnDisable() => Init();
        protected virtual void OnDestroy() => Init();

        public void EnableMouseInteractions()
        {
            var mouseInteractionEvents = GetComponent<MouseInteractionEvents>() ??
                                         gameObject.AddComponent<MouseInteractionEvents>();
            mouseInteractionEvents.onMouseClick.AddListener(_ => FireClick(LaserPointerEventArgs.Empty));
            mouseInteractionEvents.onMouseEnter.AddListener(_ => FireHover(LaserPointerEventArgs.Empty));
            mouseInteractionEvents.onMouseLeave.AddListener(_ => FireUnhover(LaserPointerEventArgs.Empty));
            mouseInteractionEvents.onMouseDown.AddListener(_ => FireSelect(LaserPointerEventArgs.Empty));
            mouseInteractionEvents.onMouseUp.AddListener(_ => FireRelease(LaserPointerEventArgs.Empty));
        }

        public void FireOnIsHolding(LaserPointerEventArgs e) => onIsHolding.Invoke(e);
        public void FireOnEndHolding(LaserPointerEventArgs e) => onEndHolding.Invoke(e);
        public void FireOnBeginHolding(LaserPointerEventArgs e) => onBeginHolding.Invoke(e);
        public void SetAllowSelection(bool flag) => allowSelection = flag;
        public void SetAllowClick(bool flag) => allowClick = flag;
        public void SetAllowHover(bool flag) => allowHover = flag;

        public void FireHover(LaserPointerEventArgs e)
        {
            if (!allowHover)
                return;

            _hoverCount++;
            onHover.Invoke(e);
            UpdateMaterial();
        }

        public void FireClick() => FireClick(LaserPointerEventArgs.Empty);
        public void FireClick(LaserPointerEventArgs e) => onClick.Invoke(e);

        public void FireUnhover(LaserPointerEventArgs e)
        {
            if (!allowHover)
                return;

            if (_hoverCount > 0)
                _hoverCount--;

            onUnhover.Invoke(e);
            UpdateMaterial();
        }

        public void FireUnhover() => FireUnhover(LaserPointerEventArgs.Empty);

        protected void FireSelect(LaserPointerEventArgs e)
        {
            if (!allowSelection)
                return;
            _selectedCount++;
            onSelect.Invoke(e);
            UpdateMaterial();
        }

        public void FireSelect() => FireSelect(LaserPointerEventArgs.Empty);

        private void FireRelease(LaserPointerEventArgs e)
        {
            _selectedCount--;
            onReleased.Invoke(e);
            UpdateMaterial();
        }

        public void FireRelease() => FireRelease(LaserPointerEventArgs.Empty);

        public void UpdateMaterial()
        {
            if (!HasMaterialUpdates)
                return;

            if (IsSelected && selectMaterial != null)
            {
                _meshRenderer.sharedMaterial = selectMaterial;
            }
            else if (IsHover && hoverMaterial != null)
            {
                _meshRenderer.sharedMaterial = hoverMaterial;
            }
            else if (_defaultMaterial != null)
            {
                _meshRenderer.sharedMaterial = _defaultMaterial;
            }
        }
        
#if UNITY_EDITOR
        private void OnValidate()
        {
            EditorUtils.HandleFlagBasedComponent<MouseInteractionEvents>(gameObject, enableDefaultMouseInteractions);
        }
#endif
    }
}