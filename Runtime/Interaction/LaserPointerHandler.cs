﻿using System;
using OmiLAXR.Adapters;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace OmiLAXR.Interaction
{
    public class LaserPointerHandler : MonoBehaviour
    { 
        private ILaserPointer _leftLaserPointer;
        private ILaserPointer _rightLaserPointer;

        [FormerlySerializedAs("OnEnterSomething")] public UnityEvent onEnterSomething = new UnityEvent();
        [FormerlySerializedAs("OnLeaveSomething")] public UnityEvent onLeaveSomething = new UnityEvent();

        public event Action<RaycastHit> OnPoint;
        public event Action OnNotPoint;
        public event Action<RaycastHit> OnHit; 
        public event Action<GameObject> OnEnterAnyPanel;
        public event Action<GameObject> OnLeaveAnyPanel;
        public event LaserPointerEventHandler OnPointerAnyIn;
        public event LaserPointerEventHandler OnPointerAnyOut;

        private Camera _mainCamera;
        
        private static LaserPointerHandler _instance;

        /// <summary>
        /// Singleton instance of the LaserPointerHandler. Only one can exist at a time.
        /// </summary>
        public static LaserPointerHandler Instance
            => _instance ??= FindObjectOfType<LaserPointerHandler>();

        public void Init(IPlayer player)
        {
            _leftLaserPointer = player.GetLeftPointer();
            _rightLaserPointer = player.GetRightPointer();
            BindEvents(_leftLaserPointer);
            BindEvents(_rightLaserPointer);

            _mainCamera = Learner.Instance.Player.GetCamera();
        }
        
        public void EnterAnyPanel(GameObject panel) => OnEnterAnyPanel?.Invoke(panel);
        public void LeaveAnyPanel(GameObject panel) => OnLeaveAnyPanel?.Invoke(panel);

        private void BindEvents(ILaserPointer laserPointer)
        {
            UnbindEvents(laserPointer);
            // Bind all events
            laserPointer.PointerClick += PointerClick;
            laserPointer.PointerIn += PointerIn;
            laserPointer.PointerOut += PointerOut;
            laserPointer.PointerBeginHold += PointerBeginHold;
            laserPointer.PointerEndHold += PointerEndHold;
            laserPointer.PointerHolding += PointerHolding;
            laserPointer.OnHit += PointerHit;

            laserPointer.OnEnterSomething += OnEnter;
            laserPointer.OnLeaveSomething += OnLeave;
        }

        private void UnbindEvents(ILaserPointer laserPointer)
        {
            // Bind all events
            laserPointer.PointerClick -= PointerClick;
            laserPointer.PointerIn -= PointerIn;
            laserPointer.PointerOut -= PointerOut;
            laserPointer.PointerBeginHold -= PointerBeginHold;
            laserPointer.PointerEndHold -= PointerEndHold;
            laserPointer.PointerHolding -= PointerHolding;
            laserPointer.OnHit -= PointerHit;

            laserPointer.OnEnterSomething -= OnEnter;
            laserPointer.OnLeaveSomething -= OnLeave;
        }

        private void PointerHit(RaycastHit hit) => OnHit?.Invoke(hit);

        private void OnLeave()
        {
            OnNotPoint?.Invoke();
            onLeaveSomething.Invoke();
        }

        private void OnEnter(RaycastHit hit)
        {
            OnPoint?.Invoke(hit);
            onEnterSomething.Invoke();
        }

        private void PointerHolding(LaserPointerEventArgs e)
        {
            var pointable = Pointable.From(e.Target);

            // break, if operation is not allowed
            if (!IsValidReaction(e.Sender, pointable)) 
                return;
            
            pointable.FireOnIsHolding(e);
        }

        private void PointerEndHold(LaserPointerEventArgs e)
        {
            var pointable = Pointable.From(e.Target);

            // break, if operation is not allowed
            if (!IsValidReaction(e.Sender, pointable))
                return;

            pointable.FireOnEndHolding(e);
        }

        private void PointerBeginHold(LaserPointerEventArgs e)
        {
            var pointable = Pointable.From(e.Target);

            // break, if operation is not allowed
            if (!IsValidReaction(e.Sender, pointable))
                return;

            pointable.FireOnBeginHolding(e);
        }

        private bool IsValidReaction(ILaserPointer sender, Pointable pointable)
        {
            if (pointable == null || sender == null)
                return false;

            return (_rightLaserPointer != null && _rightLaserPointer.Equals(sender) && pointable.reactsOnRightPointer)
                   || (_leftLaserPointer != null && _leftLaserPointer.Equals(sender) && pointable.reactsOnLeftPointer);
        }

        private void PointerIn(LaserPointerEventArgs e)
        {
            var pointable = Pointable.From(e.Target);

            // break, if operation is not allowed
            if (!IsValidReaction(e.Sender, pointable)) 
                return;

            pointable.FireHover(e);
            OnPointerAnyIn?.Invoke(e);
        }

        private void PointerClick(LaserPointerEventArgs e)
        {
            var pointable = Pointable.From(e.Target);

            // break, if operation is not allowed
            if (!IsValidReaction(e.Sender, pointable)) 
                return;

            pointable.FireClick(e);
        }

        private void PointerOut(LaserPointerEventArgs e)
        {
            var pointable = Pointable.From(e.Target);

            // break, if operation is not allowed
            if (!IsValidReaction(e.Sender, pointable)) 
                return;

            pointable.FireUnhover(e);
            OnPointerAnyOut?.Invoke(e);
        }
    }

}