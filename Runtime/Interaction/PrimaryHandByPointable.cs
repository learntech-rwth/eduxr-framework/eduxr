﻿using UnityEngine;

namespace OmiLAXR.Interaction
{
    [RequireComponent(typeof(Pointable))]
    public class PrimaryHandByPointable : MonoBehaviour
    {
        private Pointable pointable;
        public bool reactOnClick = true;
        public bool reactOnHover = true;

        // Start is called before the first frame update
        private void Awake()
        {
            pointable = GetComponent<Pointable>();

            // Recognize primary hand on click
            if (reactOnClick)
                pointable.onClick.AddListener(Handle);

            // Recognize primary hand on hover
            if (reactOnHover)
                pointable.onHover.AddListener(Handle);
        }

        /// <summary>
        /// Set automatically primary hand by interacting laser pointer.
        /// </summary>
        private void Handle(LaserPointerEventArgs e)
        {
            // Auto set primary hand
            var side = Learner.Instance.SetPrimaryHand(e.Sender);

            DebugLog.OmiLAXR.Print("[PrimaryHandByPointable]: Primary hand is " + side);

            // Remove listeners
            pointable.onClick.RemoveListener(Handle);
            pointable.onHover.RemoveListener(Handle);

            // Go sleeping. To reactivate this script, just reenable
            enabled = false;
        }
    }
}
