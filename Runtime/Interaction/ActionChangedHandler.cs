﻿
namespace OmiLAXR.Interaction
{
    /// <summary>
    /// Delegate which can be used for events where a controller action has been changed.
    /// </summary>
    public delegate void ActionChangedHandler(object sender, Learner.Body.Hand hand, string actionName, ActionState state);
}