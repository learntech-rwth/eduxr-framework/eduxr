﻿using System;
using System.Collections;
using OmiLAXR.xAPI.Extensions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace OmiLAXR.Interaction
{
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu("OmiLAXR / Interaction / Pointable UI Element (for Laser Pointer and Mouse)")]
    [DefaultExecutionOrder(10000)]
    public class PointableUI : Pointable, IPointerEnterHandler, IPointerExitHandler
    {
        private Selectable _selectable;
        
        
        [FormerlySerializedAs("OnEnabled")] public UnityEvent onEnabled = new UnityEvent();
        [FormerlySerializedAs("OnDisabled")] public UnityEvent onDisabled = new UnityEvent();

        public bool releaseSelectionAfterClick = true;
        public string pointableGroup = string.Empty;
        public GameObject groupHolder;
        public bool isDisabled;

        private bool _isAlreadyInside;

        [Tooltip("The object gets selected if user is pressing trigger. This option makes selection valid on a click (not press down).")]
        public bool selectOnClick;
        
        public int sliderPrecision = 1;

        private bool _wasDown = false;
        
        protected override void Start()
        {
            var boxCollider = gameObject.GetComponent<BoxCollider>();
            // if do not exists
            if (!boxCollider)
                StartCoroutine(AddBoxCollider());

            _selectable = GetComponent<Selectable>();

            if (isDisabled && _selectable)
                _selectable.SetDisabled(true);

            if (Learner.Instance.IsVR)
            {
                onHover.AddListener(PointerInside);
                onUnhover.AddListener(PointerOutside);
                onPressed.AddListener(PointerPressed);
                onClick.AddListener(PointerClick);
                onIsHolding.AddListener(PointerHolding);
            }


            // Auto select
            if (IsSelected && _selectable)
                _selectable.Select();

            base.Start();
        }

        protected void OnEnable()
        {
            if (!GetComponent<BoxCollider>()) StartCoroutine(AddBoxCollider());
        }

        private IEnumerator AddBoxCollider()
        {
            yield return null;
            var rectTransform = GetComponent<RectTransform>();
            var boxCollider = gameObject.AddComponent<BoxCollider>();
            var rect = rectTransform.rect;
            
            boxCollider.center = new Vector3(rect.center.x, rect.center.y, 0);
            var width = Mathf.Max(0, rect.width);
            var height = Mathf.Max(0, rect.height);
            boxCollider.size = new Vector3(width, height, 0.05f);
        }
        
        private bool IsValidUIElement => 
            _selectable != null && _selectable.interactable;

        private void UnselectAllOtherButtonsInGroup()
        {
            if (!groupHolder)
                return;

            // Iterate over all same level elements
            for (var i = 0; i < groupHolder.transform.childCount; i++)
            {
                var child = groupHolder.transform.GetChild(i);

                // skip if find itself
                if (child.gameObject == gameObject)
                    continue;

                var pointableUI = child.GetComponent<PointableUI>();

                // skip, if it is not pointable UI element or not in same group
                if (!pointableUI || pointableUI.pointableGroup != pointableGroup)
                    continue;

                // get its button and unselect it
                var selectable = pointableUI.GetComponent<Selectable>();
                if (selectable)
                    selectable.OnDeselect(null);
            }
        }

        private void PointerClick(LaserPointerEventArgs e)
        {
            // if Button disabled, skip
            if (!IsValidUIElement)
                return;
            HandleClick();
        }

        private void HandleClick()
        {
            // Select element on click
            if (selectOnClick)
                _selectable.OnSelect(null); 
             
            // Submit click event on button
            if (_selectable.GetType() == typeof(Button))
            {
                ((Button)_selectable).OnSubmit(null);
            }
            
            if (pointableGroup != string.Empty)
                UnselectAllOtherButtonsInGroup();

            if (releaseSelectionAfterClick)
                _selectable.OnDeselect(null);
            
        }

        private void PointerPressed(LaserPointerEventArgs e)
        {
            // if Button disabled, skip
            if (!IsValidUIElement)
                return;

            if (!selectOnClick)
                _selectable.OnSelect(null);

            if (_selectable.GetType() == typeof(Toggle))
            {
                var toggle = _selectable as Toggle;
                var isOn = toggle!.isOn;
                toggle.isOn = !isOn;
            }
        }

        private void PointerHolding(LaserPointerEventArgs e)
        {
            if (!IsValidUIElement)
                return;
            if (_selectable.GetType() == typeof(Slider))
            {
                var slider = _selectable as Slider;

                var sliderTransform = slider!.transform;
                if (!GetPlaneTouchPoint(e.Sender, slider.transform, out var touchPoint))
                    return;
                
                // Transform Touch Point of Plane into local space
                var localPoint = sliderTransform.InverseTransformPoint(touchPoint);

                var xPoint = localPoint.x;

                var sliderRectTransform = sliderTransform as RectTransform;
                var width = sliderRectTransform!.rect.width;
                var percent = (width / 2 + xPoint) / width;
                var sliderValue = ((slider.maxValue - slider.minValue) * percent) + slider.minValue;
                slider.value = (float) Math.Round(sliderValue, sliderPrecision, MidpointRounding.AwayFromZero);
            }

            if (_selectable.GetType() == typeof(Scrollbar))
            {
                var scrollbar = _selectable as Scrollbar;
                
                var scrollbarTransform = scrollbar!.transform;
                if (!GetPlaneTouchPoint(e.Sender, scrollbar.transform, out var touchPoint))
                    return;
                
                // Transform Touch Point of Plane into local space
                var localPoint = scrollbarTransform.InverseTransformPoint(touchPoint);
                var scrollbarRectTransform = scrollbarTransform as RectTransform;
                
                var point = 0f;
                var length = 0f;
                var sign = 1f;

                switch (scrollbar.direction)
                {
                    case Scrollbar.Direction.RightToLeft:
                        sign = -1f;
                        point = localPoint.x;
                        length = scrollbarRectTransform!.rect.width;
                        break;
                    case Scrollbar.Direction.LeftToRight:
                        point = localPoint.x;
                        length = scrollbarRectTransform!.rect.width;
                        break;
                    case Scrollbar.Direction.BottomToTop:
                        point = localPoint.y;
                        length = scrollbarRectTransform!.rect.height;
                        break;
                    case Scrollbar.Direction.TopToBottom:
                        sign = -1f;
                        point = localPoint.y;
                        length = scrollbarRectTransform!.rect.height;
                        break;
                }

                scrollbar.value = Mathf.Clamp01((length / 2 + sign * point) / length);
            }
        }

        private void PointerOutside(LaserPointerEventArgs e)
        {
            // if Button disabled, skip
            if (!IsValidUIElement || !_isAlreadyInside)
                return;
            _isAlreadyInside = false;
            _selectable.OnPointerExit(null);
        }
        
        private void PointerInside(LaserPointerEventArgs e)
        {
            // if Button disabled, skip
            if (!IsValidUIElement || _isAlreadyInside)
                return;
            _isAlreadyInside = true;
            _selectable.OnPointerEnter(null);
        }

        public void SetDisabled(bool flag)
        {
            if (flag)
                onDisabled.Invoke();
            else
                onEnabled.Invoke();

            isDisabled = flag;
            
            if (_selectable)
                _selectable.SetDisabled(flag);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _isAlreadyInside = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _isAlreadyInside = false;
        }

        // TODO: @Sergej to refactor
        private bool ProjectOnPlane(Vector3 u, Vector3 v, Vector3 planePosition, out Vector3 touchPoint, Ray ray)
            => ProjectOnPlane(planeNormal: Vector3.Cross(u, v).normalized, planePosition, out touchPoint, ray);

        private bool ProjectOnPlane(Vector3 planeNormal, Vector3 planePosition, out Vector3 touchPoint, Ray ray)
        {
            var plane = new Plane(planeNormal, planePosition);
            return ProjectOnPlane(plane, out touchPoint, ray);
        }

        private bool ProjectOnPlane(Plane plane, out Vector3 touchPoint, Ray ray)
        {
            // Reset
            touchPoint = Vector3.zero;

            // Raycast laser pointer on planes
            if (!plane.Raycast(ray, out var d))
                return false;

            // Get Touch Point on Plane
            touchPoint = ray.GetPoint(d);
            return true;
        }

        public bool GetPlaneTouchPoint(ILaserPointer laser, Transform t, out Vector3 touchPoint)
            => ProjectOnPlane(t.right, t.up, planePosition: t.position, out touchPoint, laser.GetRay());
    }
}
