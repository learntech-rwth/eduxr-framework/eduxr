﻿using UnityEngine;
using UnityEngine.Events;
using System;

namespace OmiLAXR.Interaction
{
    [DisallowMultipleComponent]
    [AddComponentMenu("OmiLAXR / Interaction / Mouse Interaction Events")]
    [Serializable]
    public class MouseInteractionEvents : MonoBehaviour
    {
        private IInteractable _interactable;

        public UnityEvent<MouseInteractionEvents> onMouseClick = new UnityEvent<MouseInteractionEvents>();
        public UnityEvent<MouseInteractionEvents> onMouseEnter = new UnityEvent<MouseInteractionEvents>();
        public UnityEvent<MouseInteractionEvents> onMouseLeave = new UnityEvent<MouseInteractionEvents>();
        public UnityEvent<MouseInteractionEvents> onMouseDown = new UnityEvent<MouseInteractionEvents>();
        public UnityEvent<MouseInteractionEvents> onMouseUp = new UnityEvent<MouseInteractionEvents>();

        private bool _wasDown;
        private bool _wasHovering;


        private void Awake()
        {
            _interactable = GetComponent<IInteractable>();
        }
        
        private void Start()
        { 
            Learner.Instance.OnLoadPlayer += player => enabled = player.XRType == XRType.Desktop;
        }

        private void OnEnable()
        {
            _wasDown = false;
            _wasHovering = false;
        }
        
        private void Update()
        {
            if (_interactable == null)
                return;
            // update current states
            var isDown = MouseInteractionHandler.Instance.IsMouseDown();
            var isHovering = MouseInteractionHandler.Instance.IsHovering(gameObject);

            // handle Hover and Unhover
            if (!_wasHovering && isHovering)
            {
                onMouseEnter.Invoke(this);
            }
            else if (_wasHovering && !isHovering)
            {
                onMouseLeave.Invoke(this);
            }

            _wasHovering = isHovering;

            if (!isDown && _wasDown)
            {
                onMouseUp.Invoke(this);
            }

            // handle click events for object
            if (isHovering)
            {
                if (!_wasDown && isDown)
                {
                    onMouseDown.Invoke(this);;
                }

                var isClick = _wasDown && !isDown;

                if (isClick)
                {
                    onMouseClick.Invoke(this);
                }
            }

            _wasDown = isDown;
        }
    }

}