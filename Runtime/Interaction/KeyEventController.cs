﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace OmiLAXR.Interaction
{
    public class KeyEventController : MonoBehaviour
    {
        [System.Serializable] 
        public struct StringEventEntry
        {
            public string notice;
            public string key;
            public UnityEvent action;
        }
    
        public bool requireSpaceBar = true;
        
        public List<StringEventEntry> events = new List<StringEventEntry>();

        public List<GameObject> gameObjectsToEnable = new List<GameObject>();

        private void Start()
        {
            var sb = new StringBuilder();
            sb.AppendLine("[KeyEventController]: Press Space+[Key] to trigger the events.");
            events.ForEach(see =>
            {
                sb.AppendLine("[KeyEventController]: Press Space+" + see.key.ToUpper() + $" to invoke '{see.notice}'.");
                // bind event to all actions
                see.action.AddListener(() => gameObjectsToEnable.ForEach(go => go.SetActive(true)));
            });
            DebugLog.OmiLAXR.Print(sb.ToString());
        }

        // Update is called once per frame
        private void Update()
        {
            var isDownCtrl = Input.GetKey(KeyCode.Space);

            if (!Input.anyKeyDown || (!isDownCtrl && requireSpaceBar)) 
                return;
            
            var key = Input.inputString.ToUpper();
            
            if (key == "" || key == " ") 
                return;

            var stringEventEntry = events.SingleOrDefault(e => e.key.ToUpper() == key);
            
            UnityEngine.Debug.Log("[KeyEventController]: Pressed Space+" + key + $". Invoked action '{stringEventEntry.notice}'.");
            stringEventEntry.action?.Invoke();
        }
    }
}
