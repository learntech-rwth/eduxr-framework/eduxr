﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace OmiLAXR.Interaction
{
    public abstract class TeleportEvents : MonoBehaviour
    {
        public UnityEvent OnVisit = new UnityEvent();
        public UnityEvent OnLeave = new UnityEvent();

        public bool IsCurrentlyVisited { get; protected set; }

        public abstract void FireTeleport();

        private void Start()
        {
            IsCurrentlyVisited = false;
        }

        private void OnEnable()
        {
            IsCurrentlyVisited = false;
        }

        protected virtual void OnTeleport(GameObject teleportTarget)
        {
            if (!teleportTarget)
                return;
            
            if (teleportTarget == gameObject)
            {
                IsCurrentlyVisited = true;
                OnVisit.Invoke();
                return;
            }

            if (IsCurrentlyVisited)
            {
                OnLeave.Invoke();   
            }
            
            IsCurrentlyVisited = false;
        }
    }
}