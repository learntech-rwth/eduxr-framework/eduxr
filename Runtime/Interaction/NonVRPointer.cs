﻿using System;
using UnityEngine;

namespace OmiLAXR.Interaction
{
    public class NonVRPointer : MonoBehaviour, ILaserPointer
    {
        public GameObject GetGameObject() => gameObject;
        public event LaserPointerEventHandler PointerClick;
        public event LaserPointerEventHandler PointerIn;
        public event LaserPointerEventHandler PointerOut;
        public event LaserPointerEventHandler PointerBeginHold;
        public event LaserPointerEventHandler PointerEndHold;
        public event LaserPointerEventHandler PointerHolding;
        public event LaserPointerEventHandler PointerHit;
        public event LaserPointerEventHandler OnEnter;
        public event LaserPointerEventHandler OnLeave;
        public event Action<RaycastHit> OnHit;
        public event Action<RaycastHit> OnEnterSomething;
        public event Action OnLeaveSomething;
        public Ray GetRay() => new Ray();
    }
}