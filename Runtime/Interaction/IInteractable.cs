﻿namespace OmiLAXR.Interaction
{
    public interface IInteractable
    {
        void SetHighlightOnHover(bool highlightValue);
        bool IsHovering();
    }
}