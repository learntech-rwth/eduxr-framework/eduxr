﻿using UnityEngine;

namespace OmiLAXR.Interaction
{
    public interface IPointable
    {
        void PointAtWorldPosition(Vector3 position);
    }

}