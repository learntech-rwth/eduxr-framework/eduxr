﻿namespace OmiLAXR.Interaction
{
    /// <summary>
    /// State of controller actions.
    /// </summary>
    public enum ActionState
    {
        Pressed,
        Released
    }
}