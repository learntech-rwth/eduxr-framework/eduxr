﻿using System;
using UnityEngine;

namespace OmiLAXR.Interaction
{
    public interface ILaserPointer
    {
        public GameObject GetGameObject();
        public event LaserPointerEventHandler PointerClick;
        public event LaserPointerEventHandler PointerIn;
        public event LaserPointerEventHandler PointerOut;
        public event LaserPointerEventHandler PointerBeginHold;
        public event LaserPointerEventHandler PointerEndHold;
        public event LaserPointerEventHandler PointerHolding;
        public event LaserPointerEventHandler PointerHit;
        public event LaserPointerEventHandler OnEnter;
        public event LaserPointerEventHandler OnLeave;
        public event Action<RaycastHit> OnHit;
        public event Action<RaycastHit> OnEnterSomething;
        public event Action OnLeaveSomething;
        public Ray GetRay();
    }
}