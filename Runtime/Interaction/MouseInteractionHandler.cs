﻿using UnityEngine;

namespace OmiLAXR.Interaction
{
    public class MouseInteractionHandler : SingletonBehaviour
    {
        private static MouseInteractionHandler _instance;
        public static MouseInteractionHandler Instance => _instance ??= FindObjectOfType<MouseInteractionHandler>();
        private Transform _mouseHoveringTransform;

        public override MonoBehaviour GetInstance()
            => Instance;
        
        private void Update()
        {
            var ray = Camera.main!.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out var hit, 100);
            _mouseHoveringTransform = hit.transform;
        }

        public bool IsMouseDown() => Input.GetMouseButton(0);
        
        public bool IsHovering(GameObject go)
            => go.transform == _mouseHoveringTransform;

    }
}