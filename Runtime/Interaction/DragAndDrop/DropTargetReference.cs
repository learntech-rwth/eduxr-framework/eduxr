﻿using UnityEngine;

namespace OmiLAXR.Interaction.DragAndDrop
{
    /// <summary>
    /// Can be used as alternative target, but redirects the interactions to its reference.
    /// </summary>
    [DefaultExecutionOrder(100)]
    public class DropTargetReference : MonoBehaviour
    {
        public DropTarget referenceToDropTarget;
        public Pointable reactsOnPointable;

        private void Start()
        {
            if (!reactsOnPointable)
                reactsOnPointable = GetComponent<Pointable>();
            
            reactsOnPointable.onClick.AddListener(HandleClick);
        }

        private void HandleClick(LaserPointerEventArgs args)
        {
            referenceToDropTarget.DropHere();
        }
    }
}