﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace OmiLAXR.Interaction.DragAndDrop
{
    [Serializable]
    [DisallowMultipleComponent]
    public class DropTarget : MonoBehaviour
    {
        public Pointable reactsOnPointable;

        public UnityEvent<Draggable> onDraggedFromHere = new UnityEvent<Draggable>();
        public UnityEvent<Draggable> onDroppedHere = new UnityEvent<Draggable>();

        [HideInInspector]
        public UnityEvent onReleasedObjects = new UnityEvent();

        protected readonly List<Draggable> HoldingObjects = new List<Draggable>();

        public bool IsFull => HoldingObjects.Count > 0 && 
                              HoldingObjects.Count == onlyAllowedDraggables.Count &&
                              HoldingObjects.Intersect(onlyAllowedDraggables).Count() == HoldingObjects.Count;

        [Tooltip("If empty, every draggable will be allowed. Otherwise only allow these draggable to drop here.")]
        public List<Draggable> onlyAllowedDraggables = new List<Draggable>();

        public static Draggable DraggingObject => DragAndDropManager.Instance.DraggingObject;
        
        protected virtual void Start()
        {
            if (!reactsOnPointable)
                reactsOnPointable = GetComponent<Pointable>();
            
            reactsOnPointable.onClick.AddListener(_ => DropHere());
            onDraggedFromHere.AddListener(DragFromHere);
        }

        // todo: need to be fixed
        public void ReleaseObjects()
        {
            var childs = HoldingObjects.ToArray();
            foreach (var c in childs)
                c.ResetDrag();
            HoldingObjects.Clear();
            onReleasedObjects.Invoke();
        }

        public virtual void DropHere()
        {
            var obj = DraggingObject;
            if (!obj)
                return;
            
            if (IsAllowedToDropHere(obj))
                DropHere(obj);
        }

        public bool IsAllowedToDropHere(Draggable draggable)
            => onlyAllowedDraggables.Count < 1 || onlyAllowedDraggables.Contains(draggable);

        protected virtual void DragFromHere(Draggable draggable)
        {
            if (HoldingObjects.Contains(draggable))
                HoldingObjects.Remove(draggable);
        }

        protected virtual void DropHere(Draggable draggable)
        {
            if (!DragAndDropManager.Instance.Drop(draggable)) 
                return;
            
            draggable.onEndedDrag.Invoke(draggable, this);
            onDroppedHere?.Invoke(draggable);
            if (!HoldingObjects.Contains(draggable))
                HoldingObjects.Add(draggable);
        }
    }
}