﻿using UnityEngine;
using UnityEngine.Events;

namespace OmiLAXR.Interaction.DragAndDrop
{
    public class DragAndDropManager : MonoBehaviour
    {
        //private float _cooldown;
        //public const float CooldownThresholdSeconds = 1.0f;
        
        private static DragAndDropManager _instance;
        /// <summary>
        /// Singleton instance of the Drag and Drop Manager. Only one can exist at a time.
        /// </summary>
        public static DragAndDropManager Instance
            => _instance ??= FindObjectOfType<DragAndDropManager>();

        [Tooltip("The place where the object will be displayed while dragging.")]
        public GameObject draggingObjectHolder;
        
        public UnityEvent<Draggable> onDraggedAny = new UnityEvent<Draggable>();
        public UnityEvent<Draggable> onDroppedAny = new UnityEvent<Draggable>();

        public Draggable DraggingObject { get; private set; }

        /// <summary>
        /// Starting dragging an draggable.
        /// </summary>
        /// <param name="draggable"></param>
        /// <returns>true, if successful</returns>
        public bool Drag(Draggable draggable)
        {
            // wait until next dragging action is possible
            //if (_cooldown > 0.0f)
            //    return false;
            
            DraggingObject = draggable;
            onDraggedAny?.Invoke(draggable);
            return true;
        }
        /// <summary>
        /// Drop a draggable.
        /// </summary>
        /// <param name="draggable"></param>
        /// <returns>true, if successful</returns>
        public bool Drop(Draggable draggable)
        {
            DraggingObject = null;
            onDroppedAny?.Invoke(draggable);
            //_cooldown = CooldownThresholdSeconds;
            return true;
        }

        //private void Update()
        //{
        //    if (_cooldown > 0.0f)
        //         _cooldown -= Time.deltaTime;
        //}
    }
}