﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace OmiLAXR.Interaction.DragAndDrop
{
    [Serializable]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(MeshFilter))]
    public class Draggable : MonoBehaviour
    {
        public Pointable reactsOnPointable;

        public bool showMiniature = true;
        public float miniatureScale = 0.2f;

        public UnityEvent<Draggable> onStartedDrag = new UnityEvent<Draggable>();
        public UnityEvent<Draggable, DropTarget> onEndedDrag = new UnityEvent<Draggable, DropTarget>();
        
        public bool hideWhileDragging = true;

        public DropTarget currentHolder;

        protected Vector3 InitialPosition;
        protected Vector3 InitialRotation;
        protected Vector3 InitialScale;
        protected Transform InitialParent;
        protected DropTarget InitialHolder;
        
        protected DropTarget DropTarget;

        protected GameObject Miniature;
        protected MeshFilter MeshFilter;

        protected virtual void Awake()
        {
            if (!reactsOnPointable)
                reactsOnPointable = GetComponent<Pointable>();
            
            reactsOnPointable.onClick.AddListener(_ => DragThis());
            
            onStartedDrag.AddListener(BeganDrag);
            onEndedDrag.AddListener(EndDrag);

            var t = transform;
            InitialHolder = currentHolder;
            InitialPosition = t.position;
            InitialRotation = t.localEulerAngles;
            InitialScale = t.localScale;
            InitialParent = t.parent;
            
            DropTarget = GetComponent<DropTarget>();
            MeshFilter = GetComponent<MeshFilter>();
        }

        public void ResetDrag()
        {
            var t = transform;
            t.position = InitialPosition;
            t.localEulerAngles = InitialRotation;
            t.localScale = InitialScale;
            currentHolder = InitialHolder;
            t.SetParent(InitialParent);
        }

        private void BeganDrag(Draggable d)
        {
            if (Learner.Instance.IsVR && showMiniature)
                CreateMiniature(d.gameObject);
            
            if (currentHolder)
                currentHolder.onDraggedFromHere.Invoke(d);

            if (d.hideWhileDragging)
                d.gameObject.SetActive(false);
        }

        private void EndDrag(Draggable d, DropTarget t)
        {
            if (Miniature)
                Destroy(Miniature);
            
            if (d.hideWhileDragging)
                d.gameObject.SetActive(true);

            if (currentHolder != t && DropTarget)
            {
                DropTarget.ReleaseObjects();
            }
            
            currentHolder = t;
        }

        protected virtual void DragThis()
        {
            if (DragAndDropManager.Instance.DraggingObject != null)
                return;
            
            if (DragAndDropManager.Instance.Drag(this))
                onStartedDrag?.Invoke(this);
        }

        protected void CreateMiniature(GameObject go)
        {
            var parent = Learner.Instance.GetPrimaryHand().GameObject.transform;
            var attachmentPoint = parent.GetComponentInChildren<MiniatureAttachmentPoint>();
            
            var attachmentPosition = new Vector3();
            var attachmentRotation = new Quaternion();
            if (attachmentPoint)
            {
                var attachmentTransform = attachmentPoint.transform; 
                attachmentPosition = attachmentTransform.position;
                attachmentRotation = attachmentTransform.rotation;
            }
            
            
            var scale = Vector3.one * miniatureScale;
            var boundsSize = MeshFilter.mesh.bounds.size;
            scale.x /= boundsSize.x;
            scale.y /= boundsSize.y;
            scale.z /= boundsSize.z;

            Miniature = Instantiate(original:go, parent: parent);
            Miniature.GetComponentInChildren<Draggable>().enabled = false;
            
            
            Miniature.transform.localScale = scale;

            const int ignoreRaycastLayer = 2; //LayerMask.GetMask("Ignore Raycast")
            SetLayers(Miniature.transform, ignoreRaycastLayer);
            Miniature.transform.rotation = attachmentRotation;
            Miniature.transform.position = attachmentPosition;
        }

        private void SetLayers(Transform parent, int layer)
        {
            foreach (var tr in parent.GetComponentsInChildren<Transform>())
                tr.gameObject.layer = layer;
        }
    }
}