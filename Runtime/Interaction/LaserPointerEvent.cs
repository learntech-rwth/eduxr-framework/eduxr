﻿using System;
using UnityEngine.Events;

namespace OmiLAXR.Interaction
{
    [Serializable]
    public class LaserPointerEvent : UnityEvent<LaserPointerEventArgs>
    {
        
    }
}