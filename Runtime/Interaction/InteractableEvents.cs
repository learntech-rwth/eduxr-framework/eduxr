﻿using UnityEngine;

namespace OmiLAXR.Interaction
{
    [DisallowMultipleComponent]
    public class InteractableEvents : MonoBehaviour
    {
        public event InteractableCollisionHandler CollisionEnter;
        public event InteractableCollisionHandler CollisionExit;
        public event InteractableTriggerHandler TriggerEnter;
        public event InteractableTriggerHandler TriggerExit;

        private void OnTriggerEnter(Collider other)
        {
            TriggerEnter?.Invoke(gameObject, other);
        }
        private void OnTriggerExit(Collider other)
        {
            TriggerExit?.Invoke(gameObject, other);
        }
        private void OnCollisionEnter(Collision collision)
        {
            CollisionEnter?.Invoke(gameObject, collision);
        }

        private void OnCollisionExit(Collision collision)
        {
            CollisionExit?.Invoke(gameObject, collision);
        }

        public delegate void InteractableCollisionHandler(GameObject sender, Collision collision);
        public delegate void InteractableTriggerHandler(GameObject sender, Collider collider);
    }

}