﻿using UnityEngine;

namespace OmiLAXR.Interaction
{
    public struct LaserPointerEventArgs
    {
        public RaycastHit? Hit;
        public Transform Target;
        public float Distance;
        public ILaserPointer Sender;
        public static LaserPointerEventArgs Empty = new LaserPointerEventArgs();

        public LaserPointerEventArgs(ILaserPointer sender, Transform target, float distance, RaycastHit? hit)
        {
            Sender = sender;
            Target = target;
            Distance = distance;
            Hit = hit;
        }
    }
}