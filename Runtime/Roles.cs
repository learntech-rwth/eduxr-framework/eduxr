﻿namespace OmiLAXR
{
    public enum Roles
    {
        Other = 0,
        Learner = 1,
        Guide = 2,
        Collaborator = 4
    }
}