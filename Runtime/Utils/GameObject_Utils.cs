﻿using UnityEngine;

using OmiLAXR.Extensions;

namespace OmiLAXR.Utils
{
    public class GameObject_Utils
    {
        public static GameObject CreateLine(Vector3 from, Vector3 to, float? thickness = null, GameObject holder = null)
        {
            var line = CreateLine(holder ? holder.transform : null);
            var distance = Vector3.Distance(from, to);
            line.transform.position = from;
            line.transform.LookAt(to);
            var s = thickness.HasValue ? line.transform.localScale : thickness.Value * Vector3.one;
            line.transform.localScale = new Vector3(s.x, s.y, distance);
            return line;
        }
        public static GameObject CreateLine(Transform parent = null)
        {

            var line = new GameObject("Line");
            var lineMeshObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            lineMeshObject.name = "Line Mesh";
            lineMeshObject.GetComponent<BoxCollider>()?.Destroy();

            lineMeshObject.transform.localPosition = new Vector3(0, 0, 0.5f);
            lineMeshObject.transform.localScale = new Vector3(0.0025f, 0.0025f, 1f);

            lineMeshObject.transform.SetParent(line.transform);
            line.transform.SetParent(parent);
            return line;
        }
    }

}