﻿using UnityEngine;

namespace OmiLAXR.Utils
{
    public static class Transform_Utils
    {
        public static void PlaceLine(Transform line, Vector3 from, Vector3 to)
        {
            var distance = Vector3.Distance(from, to);

            var parent = line.transform.parent;

            var worldTo = parent.TransformPoint(to);

            line.transform.localPosition = from;
            line.transform.LookAt(worldTo);

            line.transform.localScale = new Vector3(1.0f, 1.0f, distance);
        }
    }
}
