﻿using System;
using System.Linq;
using UnityEngine;

namespace OmiLAXR.Utils
{
    public static class Vector3_Utils
    {
        public static bool IsInsideSphere(Vector3 point, Vector3 centerOfSphere, float radiusOfSphere)
            => Math.Sqrt((point.x - centerOfSphere.x) + (point.y - centerOfSphere.y) + (point.z - centerOfSphere.z)) <=
               radiusOfSphere * radiusOfSphere;
        public static Vector3 CenterOfGravity(Vector3[] points)
        {
            if (points.Length < 1)
                return Vector3.zero;

            var sum = points.Aggregate(Vector3.zero, (current, p) => current + p);
            return sum / points.Length;
        }

        public static float AverageDistance(Vector3 from, Vector3[] points)
        {
            if (points.Length < 1)
                return 0;

            var distance = points.Sum(p => Vector3.Distance(from, p));

            return distance / points.Length;
        }

        public static float SmallestMid(Vector3 from, Vector3[] points)
        {
            if (points.Length < 1)
                return 0;

            var dis = float.MaxValue;
            for (var i = 0; i < points.Length; i++)
            {
                var cur = points[i];
                var next = points[(i + 1) % points.Length];
                var dir = next - cur;
                var mid = cur + dir / 2;
                var d = Vector3.Distance(from, mid);
                dis = Mathf.Min(d, dis);
            }
            return dis;
        }
    }

}