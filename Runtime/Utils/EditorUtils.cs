﻿#if UNITY_EDITOR
using OmiLAXR.Extensions;
using UnityEditor;
using UnityEngine;

namespace OmiLAXR.Utils
{

    public static class EditorUtils
    {
        public static void AddComponentToSelection<T>() where T : MonoBehaviour
            => AddComponentToGameObject<T>(Selection.activeGameObject);
        public static void AddComponentToGameObject<T>(GameObject target) where T : MonoBehaviour
        {
            if (!target || target.GetComponent<T>())
                return;
            target.AddComponent<T>();
        }
        /// <summary>
        /// Add prefab to active selection.
        /// </summary>
        /// <param name="prefabPath">Prefab path</param>
        /// <param name="fallbackToRoot">If true and Selection is null, then add to root</param>
        public static void AddPrefabToSelection(string prefabPath, bool fallbackToRoot = false) => AddPrefab(prefabPath, Selection.activeGameObject, fallbackToRoot);
        /// <summary>
        /// Add prefab to selected game object.
        /// </summary>
        /// <param name="prefabPath">Path of prefab</param>
        /// <param name="target">Target</param>
        /// <param name="fallbackToRoot">If true and Selection is null, then add to root</param>
        public static void AddPrefab(string prefabPath, GameObject target = null, bool fallbackToRoot = false)
        {
            var prefab = Resources.Load<GameObject>(prefabPath);
            
            if (target)
            {
                DebugLog.OmiLAXR.Print($"'{prefab.name}' was added to '{target!.name}'.");
                PrefabUtility.InstantiatePrefab(prefab, target!.transform);
            }
            else if (fallbackToRoot)
            {
                DebugLog.OmiLAXR.Print($"'{prefab.name}' was added to the root of the project.");
                PrefabUtility.InstantiatePrefab(prefab);
            }
        }

        public static void AddPrefabToRoot(string prefabPath)
            => AddPrefab(prefabPath, null, true);

        /// <summary>
        /// Add prefab with the name 'name' to the object in scene with the type 'T'.
        /// </summary>
        /// <param name="prefabPath">Name of prefab</param>
        /// <typeparam name="T">Target type</typeparam>
        /// <param name="fallbackToRoot">If true and Selection is null, then add to root</param>
        public static void AddPrefabTo<T>(string prefabPath, bool fallbackToRoot = false) where T : MonoBehaviour
        {
            var target = Object.FindObjectOfType<T>();
            
            if (!target)
            {
                DebugLog.OmiLAXR.Error($"Cannot find a GameObject with the type '{typeof(T)}'.");
                return;
            }
            
            AddPrefab(prefabPath, target.gameObject, fallbackToRoot);
        }

        /// <summary>
        /// Recommended to be used inside Validate() method.
        /// </summary>
        /// <param name="gameObject">Object on which the component will be applied.</param>
        /// <param name="flag">Flag on which the component will be added or disabled.</param>
        /// <typeparam name="T">Component type</typeparam>
        public static void HandleFlagBasedComponent<T>(GameObject gameObject, bool flag) where T : MonoBehaviour
        {
            if (Application.isPlaying || gameObject == null)
                return;
            
            var comp = gameObject.AddOrGetComponent<T>();
            comp.enabled = flag;
            EditorUtility.SetDirty(gameObject);
        }
    }
}
#endif