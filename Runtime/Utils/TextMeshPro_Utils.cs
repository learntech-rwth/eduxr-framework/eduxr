﻿using UnityEngine;
using TMPro;

namespace OmiLAXR.Utils
{
    public static class TextMeshPro_Utils
    {
        public static TextMeshProUGUI GetTextMesh(GameObject holder, string textObjName = "Text")
        {
            var textObj = holder.transform.Find(textObjName);
            return textObj.GetComponent<TextMeshProUGUI>();
        }

        public static void SetText(GameObject holder, string text)
        {
            var textMesh = holder.GetComponent<TextMeshProUGUI>();
            textMesh.text = text;
        }

    }
}
