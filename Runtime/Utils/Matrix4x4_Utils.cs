﻿using UnityEngine;
using System.Collections;

namespace OmiLAXR.Utils
{
    public static class Matrix4x4_Utils
    {
        public static Matrix4x4 CreateOpenGLProjectionMatrix(float near, float far, float left, float right, float top, float bottom)
        {
            var n = near;
            var f = far;
            var l = left;
            var r = right;
            var t = top;
            var b = bottom;

            return new Matrix4x4(
                new Vector4(2 * n / (r - l), 0, 0, 0), // column 1
                new Vector4(0, 2 * n / (t - b), 0, 0), // column 2
                new Vector4((r + l) / (r - l), (t + b) / (t - b), -(f + n) / (f - n), -1), // column 3
                new Vector4(0, 0, -2 * f * n / (f - n), 0) // column 4
            );
        }

        public static Matrix4x4 CreateDirectXProjectionMatrix(float near, float far, float left, float right, float top, float bottom)
        {
            var n = near;
            var f = far;
            var l = left;
            var r = right;
            var t = top;
            var b = bottom;

            return new Matrix4x4(
                new Vector4(2 * n / (r - l), 0, 0, 0), // column 1
                new Vector4(0, 2 * n / (t - b), 0, 0), // column 2
                new Vector4(-(r + l) / (r - l), -(t + b) / (t - b), (f + n) / (f - n), 1), // column 3
                new Vector4(0, 0, -2 * f * n / (f - n), 0) // column 4
            );
        }
    }

}