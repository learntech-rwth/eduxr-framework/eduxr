﻿using UnityEngine;

namespace OmiLAXR.Utils
{
    public class Material_Utils
    {
        private static readonly int MainTex = Shader.PropertyToID("_MainTex");

        public static Material CreateTexturedMaterial(Texture texture)
        {
            var mat = new Material(StandardShader_Utils.CreateStandardShader());
            mat.SetTexture(MainTex, texture);
            return mat;
        }
    }

}