﻿using System.Collections.Generic;

namespace OmiLAXR.Utils
{
    public static class List_Utils
    {
        public static void RemoveArrayObjectFromList<T>(IEnumerable<T> objects, List<T> list)
        {
            foreach (var o in objects)
            {
                list.Remove(o);
            }
        }

    }

}