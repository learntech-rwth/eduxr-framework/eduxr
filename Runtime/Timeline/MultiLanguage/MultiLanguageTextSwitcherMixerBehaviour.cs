using UnityEngine;

using OmiLAXR.Extensions;
using UnityEngine.Playables;
using UnityEngine.UI;

namespace OmiLAXR.Timeline.MultiLanguage
{
    public class MultiLanguageTextSwitcherMixerBehaviour : PlayableBehaviour
    {
        private Color _defaultColor;
        private int _defaultFontSize;
        private string _defaultText;

        private Text _trackBinding;
        private bool _firstFrameHappened;
	    public AudioSource m_voiceSource;
	    public AudioClip[] m_voices;

        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            _trackBinding = playerData as Text;

            if (_trackBinding?.gameObject?.GetComponent<AudioSource>() != null){
                m_voiceSource  = _trackBinding.gameObject.GetComponent<AudioSource>();
            }
            
            if (_trackBinding == null)
                return;

            if (!_firstFrameHappened)
            {
                _defaultColor = _trackBinding.color;
                _defaultFontSize = _trackBinding.fontSize;
                _defaultText = _trackBinding.text;
                _firstFrameHappened = true;
            }

            var inputCount = playable.GetInputCount();

            var blendedColor = Color.clear;
            var blendedFontSize = 0f;
            var totalWeight = 0f;
            var greatestWeight = 0f;
            var currentInputs = 0;
            for (var i = 0; i < inputCount; i++)
            {
                var inputWeight = playable.GetInputWeight(i);
                var inputPlayable = (ScriptPlayable<MultiLanguageTextSwitcherBehaviour>)playable.GetInput(i);
                var input = inputPlayable.GetBehaviour();

                blendedColor += input.color * inputWeight;
                blendedFontSize += input.fontSize * inputWeight;
                totalWeight += inputWeight;

                if (inputWeight > greatestWeight)
                {
                    if (_trackBinding != null && input != null && LearningEnvironment.Instance != null)
                    {
                        HandleText(input);
                        HandleAudio(input);
                    }
                    
                    greatestWeight = inputWeight;
                }

                if (!Mathf.Approximately(inputWeight, 0f))
                    currentInputs++;
            }

            _trackBinding.color = blendedColor + _defaultColor * (1f - totalWeight);
            _trackBinding.fontSize = Mathf.RoundToInt(blendedFontSize + _defaultFontSize * (1f - totalWeight));
            if (currentInputs != 1 && 1f - totalWeight > greatestWeight)
            {
                _trackBinding.text = _defaultText;
            }
        }

        private void HandleText(MultiLanguageTextSwitcherBehaviour input)
        {
            var isVR = Learner.Instance && Learner.Instance.IsVR;
            var text = _trackBinding.text = input.texts.GetText(LearningEnvironment.Instance.language, !isVR);
            LearningEnvironment.Instance.ProvideNewInstruction(text);
        }
        

        private void HandleAudio(MultiLanguageTextSwitcherBehaviour input)
        {
            var isVR = Learner.Instance && Learner.Instance.IsVR;
            var audio = input.texts.GetAudio(LearningEnvironment.Instance.language,!isVR);
            
            if ((!input.audioSource && !m_voiceSource)) 
                return;
            
            if (audio != null)
            {
                PlaySound(input.audioSource, audio, false);
                return;
            }
            if (m_voices.Length > 0)
                PlaySound(input.audioSource, m_voices[Random.Range(0, m_voices.Length)], true);
        }
        

        private void PlaySound(AudioSource audioSource ,AudioClip clip, bool dontInterrupt)
        {
            if (!audioSource)
                audioSource = m_voiceSource;
            
            if ((audioSource.isPlaying || m_voiceSource.isPlaying) && dontInterrupt) return;

            audioSource.clip = clip;
            audioSource.Play();
        }

        public override void OnPlayableDestroy(Playable playable)
        {
            _firstFrameHappened = false;

            if (_trackBinding == null)
                return;

            _trackBinding.color = _defaultColor;
            _trackBinding.fontSize = _defaultFontSize;
            _trackBinding.text = _defaultText;
        }
    }
}
