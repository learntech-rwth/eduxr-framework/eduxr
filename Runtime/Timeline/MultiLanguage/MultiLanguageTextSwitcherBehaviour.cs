﻿using System;
using UnityEngine;
using UnityEngine.Playables;

namespace OmiLAXR.Timeline.MultiLanguage
{
    [Serializable]
    public class MultiLanguageTextSwitcherBehaviour : PlayableBehaviour
    {
        public Color color = Color.white;
        public int fontSize = 14;
        public MultiLanguageEntry[] texts;
        [HideInInspector] public AudioSource audioSource;
    }
}