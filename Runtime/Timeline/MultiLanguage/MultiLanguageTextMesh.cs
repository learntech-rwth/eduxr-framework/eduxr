﻿using System.Linq;
using TMPro;
using UnityEngine;

namespace OmiLAXR.Timeline.MultiLanguage
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class MultiLanguageTextMesh : MonoBehaviour
    {
        public MultiLanguageEntry[] texts;

        private void Awake()
        {
            UpdateLanguage(LearningEnvironment.Instance.language);
            RebindEvent();
        }
        private void OnEnable() => RebindEvent();
        private void OnDestroy() => UnbindEvent();
        private void OnDisable() => UnbindEvent();

        private void UnbindEvent()
        {
            if (!LearningEnvironment.HasInstance)
                return;

            LearningEnvironment.Instance.OnChangedLanguage -= UpdateLanguage;
        }

        private void RebindEvent()
        {
            LearningEnvironment.Instance.OnChangedLanguage -= UpdateLanguage;
            LearningEnvironment.Instance.OnChangedLanguage += UpdateLanguage;
        }

        private void UpdateLanguage(MultiLanguageLanguages language)
        {
            var text = GetText(language);
            GetComponent<TextMeshProUGUI>().text = text;
        }

        public string GetText(MultiLanguageLanguages language) => texts.FirstOrDefault(e => e.language == language).text;
    }
}