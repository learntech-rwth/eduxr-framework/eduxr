﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace OmiLAXR.Timeline.MultiLanguage
{
    [TrackColor(0.1394896f, 0.4411765f, 0.3413077f)]
    [TrackClipType(typeof(MultiLanguageTextSwitcherClip))]
    [TrackBindingType(typeof(Text))]
    public class MultiLanguageTextSwitcherTrack : TrackAsset
    {
		[Tooltip("Add robot voice samples here to give Kyle randomized speech for this track.")]
		public AudioClip[] voices;
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            var playable = ScriptPlayable<MultiLanguageTextSwitcherMixerBehaviour>.Create(graph, inputCount);
			playable.GetBehaviour().m_voices = voices;
			return playable;
        }

        public override void GatherProperties(PlayableDirector director, IPropertyCollector driver)
        {
#if UNITY_EDITOR
            var trackBinding = director.GetGenericBinding(this) as Text;
            if (trackBinding == null)
                return;

            var serializedObject = new SerializedObject(trackBinding);
            var iterator = serializedObject.GetIterator();
            while (iterator.NextVisible(true))
			{
                if (iterator.hasVisibleChildren)
                    continue;

                driver.AddFromName<Text>(trackBinding.gameObject, iterator.propertyPath);
            }
#endif
            base.GatherProperties(director, driver);
        }
    }
}
