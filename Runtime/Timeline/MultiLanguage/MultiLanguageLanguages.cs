﻿using System;

namespace OmiLAXR.Timeline.MultiLanguage
{
    public enum MultiLanguageLanguages
    {
        English, 
        German
    }

    public static class MultiLanguageLanguages_Ext
    {
        public static string GetLanguageCode(this MultiLanguageLanguages lang)
        {
            
            switch (lang)
            {
                case MultiLanguageLanguages.English:
                    return "en";
                case MultiLanguageLanguages.German:
                    return "de";
                default:
                    throw new ArgumentOutOfRangeException(nameof(lang), lang, null);
            }
        }
    }
}