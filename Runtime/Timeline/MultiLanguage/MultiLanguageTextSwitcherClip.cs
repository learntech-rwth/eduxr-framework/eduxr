﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace OmiLAXR.Timeline.MultiLanguage
{
    [Serializable]
    public class MultiLanguageTextSwitcherClip : PlayableAsset, ITimelineClipAsset
    { 
        public MultiLanguageTextSwitcherBehaviour template = new MultiLanguageTextSwitcherBehaviour();
        public ExposedReference<AudioSource> audioSource; 
        public ClipCaps clipCaps => ClipCaps.Blending;

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<MultiLanguageTextSwitcherBehaviour>.Create(graph, template);
            var behavior = playable.GetBehaviour();
            behavior.audioSource = audioSource.Resolve(graph.GetResolver());
            return playable;
        }
    }
}