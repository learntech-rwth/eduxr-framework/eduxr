﻿using System;
using UnityEngine;

namespace OmiLAXR.Timeline.MultiLanguage
{
    [Serializable]
    public struct MultiLanguageEntry
    {
        public MultiLanguageLanguages language;
        [TextArea(minLines: 6, maxLines: 10)]
        public string text;

        public AudioClip textAudio;
        [TextArea(minLines: 6, maxLines: 10)]
        public string desktopVariantText;

        public AudioClip desktopAudio;
        

        public MultiLanguageEntry(MultiLanguageLanguages language, string text, AudioClip textAudio, string desktopVariantText, AudioClip desktopAudio)
        {
            this.language = language;
            this.text = text;
            this.textAudio = textAudio;
            this.desktopVariantText = desktopVariantText;
            this.desktopAudio = desktopAudio;
        }

        public override string ToString() => $"[MultiLanguageEntry: language={language}, text={text}, desktopVariantText={desktopVariantText}]";

        public override bool Equals(object obj)
        {
            var mObj = (MultiLanguageEntry) obj;
            return text == mObj.text && language == mObj.language;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((int)language * 397) ^ text.GetHashCode();
            }
        }
    }
}
