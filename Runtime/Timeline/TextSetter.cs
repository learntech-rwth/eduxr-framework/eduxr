﻿using OmiLAXR.Extensions;
using OmiLAXR.Timeline.MultiLanguage;
using UnityEngine;
using UnityEngine.UI;

namespace OmiLAXR.Timeline
{
    /// <summary>
    /// Set text based on selected language.
    /// </summary>
    public class TextSetter : MonoBehaviour
    {
        [SerializeField]
        public MultiLanguageEntry[] entries;
        public Text target;

        public void SetText()
        {
            if (target != null)
                target.text = entries.GetText(LearningEnvironment.Instance.language);
        }
    }

}