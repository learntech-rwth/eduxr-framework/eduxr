using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

namespace OmiLAXR.Timeline
{
    /// <summary>
    /// Was used to wait for actions in timeline.
    /// </summary>
    [RequireComponent(typeof(Button))]
    public class PlayPauseDirector : MonoBehaviour
    {
        public PlayableDirector director;
        private bool _isWaitingForAction;
        private GameObject _hourglass;

        private readonly List<string> _waitingList = new List<string>();

        public void SetDirector(PlayableDirector d)
        {
            director = d;
            _isWaitingForAction = false;
            _waitingList.Clear();
            d.played -= OnPlayed;
            d.played += OnPlayed;
            d.stopped -= OnStopped;
            d.stopped += OnStopped;
        }

        private void OnStopped(PlayableDirector _) => IsNotWaitingAnymore();
        private void OnPlayed(PlayableDirector _) => IsNotWaitingAnymore();

        public void DoneAction()
        {
            if (!_isWaitingForAction)
                return;

            _isWaitingForAction = false;
            Continue();
        }

        public void DoneAction(string actionName)
        {
            // not waiting to this event
            if (!_waitingList.Contains(actionName))
                return;
            var index = _waitingList.IndexOf(actionName);
            _waitingList.RemoveAt(index);
            
            if (_waitingList.Count < 1)
                Continue();
        }

        public void IsNotWaitingAnymore()
        {
            _isWaitingForAction = false;
            _waitingList.Clear();
            ShowHourglass(false);
        }
        /// <summary>
        /// Waits until an action was done.
        /// </summary>
        public void WaitForAction()
        {
            _isWaitingForAction = true;
            Pause();
        }

        public void WaitForAction(string actionName)
        {
            if (!_waitingList.Contains(actionName))
                _waitingList.Add(actionName);
            Pause();
        }

        private void Start()
        {
            GetComponent<Button>().onClick.AddListener(Continue);
            _hourglass = gameObject.transform.GetChild(0).gameObject;
        }

        public void Pause()
        {
            director.Pause();
            ShowHourglass(true);
        }

        public void Continue()
        {
            if (_isWaitingForAction || _waitingList.Count > 0)
                return;

            director.Resume();
            ShowHourglass(false);
        }

        private void ShowHourglass(bool showHourglass)
        {
            if (!_hourglass)
             return;

            _hourglass.SetActive(showHourglass);
        }
    }

}
