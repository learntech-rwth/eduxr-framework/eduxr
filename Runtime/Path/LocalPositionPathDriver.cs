﻿using UnityEngine;

namespace OmiLAXR.Path
{
    public class LocalPositionPathDriver : PathDriver
    {
        protected override Vector3 GetValue() => transform.localPosition;
        protected override void SetValue(Vector3 value) => transform.localPosition = value;
    }
}