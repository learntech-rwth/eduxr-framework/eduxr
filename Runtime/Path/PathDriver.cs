﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace OmiLAXR.Path
{
    public abstract class PathDriver : MonoBehaviour
    {
        [Tooltip("Flag, which defines if this animation is running in loop or not.")]
        public bool isLoop = true;

        [Tooltip("Duration in seconds, how long the whole animation will take.")]
        public float durationInSeconds = 1.0f;

        [Tooltip("Path of positions, where the camera should slide to.")]
        public List<Vector3> path = new List<Vector3>();

        [Tooltip("Start animation automatically.")]
        public bool autostart = true;

        [Tooltip("If 0, then it will be called infinite times.")]
        public int maxEventCalls = 0;
        private int _eventCalls = 0;

        [Tooltip("Prepend start position to path.")]
        public bool prependStartPosition = true;
        
        // Current state values
        private Vector3 CurrentValue => path[_currentPathIndex];
        private Vector3 NextValue => path[(_currentPathIndex + 1) % path.Count];
        private float SecondsPerStep => path.Count < 2 ? durationInSeconds : durationInSeconds / path.Count;
        private Vector3 StartValue { get; set; }
        private bool IsRunning { get; set; }

        // Animation values
        private int _currentPathIndex = 0;
        private float _animationElapsedSeconds;
        private float _wholeAnimationElapsedSeconds;

        public UnityEvent onFinishAnimation;

        private void Start()
        {
            // Save start position
            StartValue = GetValue();

            // Prepend start position to path
            if (prependStartPosition)
                path.Insert(0, StartValue);

            // Start animation automatically
            if (autostart)
                Play();
        }

        protected abstract Vector3 GetValue();
        protected abstract void SetValue(Vector3 value);

        // Update is called once per frame
        private void Update()
        {
            // if there is no path, then do nothing
            if (!IsRunning || path.Count < 1)
                return;

            // Count elapsed seconds in overall
            _wholeAnimationElapsedSeconds += Time.deltaTime;

            // Animation is complete
            if (_wholeAnimationElapsedSeconds >= durationInSeconds)
            {
                if (maxEventCalls == 0)
                    onFinishAnimation.Invoke();
                else if (maxEventCalls > 0 && _eventCalls < maxEventCalls)
                {
                    onFinishAnimation.Invoke();
                    _eventCalls++;
                }

                // Restart animation if loop
                if (isLoop)
                    Restart();
                else
                {
                    // Don't continue otherwise
                    Pause();
                    return;
                }
            }

            // Count elapsed seconds for current animation
            _animationElapsedSeconds += Time.deltaTime;

            // Compute "t"
            var t = _animationElapsedSeconds / SecondsPerStep;

            if (t >= 1.0f)
            {
                // Get index according to elapsed time
                _currentPathIndex++;
                _animationElapsedSeconds = 0.0f;
                return;
            }

            // Interpolate position
            var value = Vector3.Lerp(CurrentValue, NextValue, t);

            SetValue(value);
        }

        public void Play()
        {
            Restart();
            IsRunning = true;
        }

        public void Stop()
        {
            IsRunning = false;
            Restart();
        }

        public void Resume() => IsRunning = true;

        public void Pause() => IsRunning = false;

        public void SetLoop(bool value) => isLoop = value;

        public void SetCalledEventCalls(int calls) => _eventCalls = calls;

        /// <summary>
        /// Restart animation.
        /// </summary>
        public void Restart()
        {
            SetValue(StartValue);

            // Reset other values
            _currentPathIndex = 0;
            _animationElapsedSeconds = 0;
            _wholeAnimationElapsedSeconds = 0;
        }
    }
}
