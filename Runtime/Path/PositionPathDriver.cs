﻿using UnityEngine;

namespace OmiLAXR.Path
{
    public class PositionPathDriver : PathDriver
    {
        protected override Vector3 GetValue() => transform.position;
        protected override void SetValue(Vector3 value) => transform.position = value;
    }
}