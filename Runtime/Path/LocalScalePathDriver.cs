﻿using UnityEngine;

namespace OmiLAXR.Path
{
    public class LocalScalePathDriver : PathDriver
    {
        protected override Vector3 GetValue() => transform.localScale;
        protected override void SetValue(Vector3 value) => transform.localScale = value;
    }
}
