﻿using UnityEngine;

namespace OmiLAXR.Path
{
    public class LocalEulerAnglesPathDriver : PathDriver
    {
        protected override void SetValue(Vector3 value) => transform.localEulerAngles = value;
        protected override Vector3 GetValue() => transform.localEulerAngles;
    }
}