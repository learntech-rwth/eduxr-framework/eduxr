﻿using UnityEngine;

namespace OmiLAXR.Path
{
    public class EulerAnglesPathDriver : PathDriver
    {
        protected override void SetValue(Vector3 value) => transform.eulerAngles = value;
        protected override Vector3 GetValue() => transform.eulerAngles;
    }
}