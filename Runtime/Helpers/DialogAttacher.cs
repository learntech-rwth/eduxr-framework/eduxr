﻿using System.ComponentModel;
using OmiLAXR.Adapters;
using UnityEngine;

namespace OmiLAXR.Helpers
{
    /// <summary>
    /// Dialog attaches to a transform and follows it.
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class DialogAttacher : MonoBehaviour, IAttacher
    {
        public Roles attachToRole = Roles.Other;
        [Tooltip("The transform to attach to.")]
        [Description("The transform to attach to.")]
        public Transform attachedTo;
        
        private RectTransform _rectTransform;

        public float yPosition = 1.0f;
        public float distance = 0.5f;
        public float degreePosition = 90.0f;

        public bool useRectTransform = true;

        protected virtual void Start()
        {
            if (useRectTransform)
                _rectTransform = GetComponent<RectTransform>();

            if (attachToRole != Roles.Other)
                attachedTo = LearningEnvironment.Instance.GetTransformFromRole(attachToRole);

            UpdateTransform();
            
            if (!attachedTo)
            {
                enabled = false;
                DebugLog.OmiLAXR.Error($"[DialogAttacher] The property 'attachedTo' is not set on object '{name}'. The component has been disabled.");
            }
        }

        private Vector3 GetOffsetPosition()
        {
            // Todo: auto compute degreePosition followed by a provided transform
            var rad = Mathf.Deg2Rad * degreePosition;
            var pos2D = new Vector2(Mathf.Cos(rad), Mathf.Sin(rad)) * distance;
            return new Vector3(pos2D.x, yPosition, pos2D.y);
        }

        protected virtual void FixedUpdate()
        {
            UpdateTransform();
        }

        private void UpdateTransform()
        {
            var offset = GetOffsetPosition();
            if (useRectTransform && _rectTransform)
                _rectTransform.transform.position = attachedTo.position + offset;
            else
                transform.position = attachedTo.position + offset;
        }

        public void AttachTo(Transform t)
        {
            attachedTo = t;
        }
    }

}