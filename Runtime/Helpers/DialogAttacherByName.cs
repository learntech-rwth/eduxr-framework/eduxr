﻿using UnityEngine;

namespace OmiLAXR.Helpers
{
    public class DialogAttacherByName : DialogAttacher
    {
        public string attachToName;
        public bool keepAttaching = true;
        protected override void Start()
        {
            base.Start();
            UpdateAttachment();
        }

        private void UpdateAttachment() => attachedTo = GameObject.Find(attachToName)?.transform;

        protected override void FixedUpdate()
        {
            if (!keepAttaching)
                return;
            if (!attachedTo)
                UpdateAttachment();
            base.FixedUpdate();
        }
    }
}