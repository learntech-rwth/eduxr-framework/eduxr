﻿using UnityEngine;

namespace OmiLAXR.Helpers
{
    public class UnpackGameObjects : MonoBehaviour
    {
        private void Awake()
        {
            for (var i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).transform.parent = transform.parent;
            }
            DestroyImmediate(this);
        }
    }
}