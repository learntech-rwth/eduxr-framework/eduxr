﻿using UnityEngine;

namespace OmiLAXR.Helpers
{
    public class RotationAround : MonoBehaviour
    {
        public bool rotateAroundXAxis;
        public bool rotateAroundYAxis;
        public bool rotateAroundZAxis;

        public float speed = 20.0f;

        public Vector3 around;
        
        public bool resetOnEnable;
        private Vector3 _startRotation;

        private void Start()
        {
            var t = transform;
            around = t.position;
            _startRotation = t.localEulerAngles;
        }

        private void OnEnable()
        {
            if (resetOnEnable)
                transform.localEulerAngles = _startRotation;
        }

        // Update is called once per frame
        private void Update()
        {
            // Rotate around X
            if (rotateAroundXAxis)
                transform.RotateAround(around, Vector3.right, speed * Time.deltaTime);
            // Rotate around Y
            if (rotateAroundYAxis)    
                transform.RotateAround(around, Vector3.up, speed * Time.deltaTime);
            // Rotate around Z
            if (rotateAroundZAxis)
                transform.RotateAround(around, Vector3.forward, speed * Time.deltaTime);
        }
    }

}