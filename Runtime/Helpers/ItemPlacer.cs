﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OmiLAXR.Helpers
{
    /// <summary>
    /// Allows generating and placing game objects relative to a 
    /// parent transform.
    /// </summary>
    public class ItemPlacer : MonoBehaviour
    {
        [SerializeField]
        private GameObject prefab;

        /// <summary>
        /// Offset from parent transform.
        /// </summary>
        public Vector3 offset;

        /// <summary>
        /// Called when object was just generated. Supplies newly generated
        /// object as parameter to the listener.
        /// </summary>
        public event Action<GameObject> generated;

        /// <summary>
        /// References the currently used item.
        /// </summary>]
        public GameObject Item { get; private set; }

        /// <summary>
        /// Changes the currently used prefab.
        /// </summary>
        /// <param name="prefab">new prefab to use</param>
        public void SetPrefab(GameObject prefab)
        {
            if (Item) DestroyImmediate(Item);

            this.prefab = prefab;
            if (prefab) GenerateItem();
        }

        /// <summary>
        /// Places an item at the current position and generates a new one.
        /// </summary>
        public void PlaceItem(bool refresh = false)
        {
            Item.transform.parent = null;

            if (refresh)
            {
                GenerateItem();
            }
            else
            {
                Item = null;
            }
        }

        private void GenerateItem()
        {
            Item = Instantiate(prefab, transform);
            Item.transform.localPosition = offset;
            generated?.Invoke(Item);
        }
    }
}