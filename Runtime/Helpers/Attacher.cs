﻿using System.ComponentModel;
using UnityEngine;

namespace OmiLAXR.Helpers
{
    /// <summary>
    /// Allows attaching and removing a prefab instance from transform.
    /// </summary>
    public class Attacher : MonoBehaviour
    {
        [Tooltip("Reference to a prefab or scene object to clone.")]
        [Description("Reference to a prefab or scene object to clone.")]
        public GameObject prefab;
        private GameObject _instance;

        [Description("Determines whether a instance is currently attached to something.")]
        public bool IsAttached => _instance != null;

        /// <summary>
        /// Attaches game object to transform
        /// </summary>
        /// <param name="to">target transform</param>
        public GameObject AttachTo(Transform to)
        {
            if (_instance) Detach();
            _instance = Instantiate(prefab, to);
            return _instance;
        }

        /// <summary>
        /// Removes instance from the 
        /// </summary>
        public void Detach()
        {
            if (!_instance)
            {
                DebugLog.OmiLAXR.Warning("Trying to detach non-existing object.");
            }
            else
            {
                _instance.transform.parent = null;
                Destroy(_instance);
            }
        }
    }
}