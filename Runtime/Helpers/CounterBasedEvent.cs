﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace OmiLAXR.Helpers
{
    public class CounterBasedEvent : MonoBehaviour
    {
        public uint counter = 0;
        public uint fireEventAtCounter = 0;
        public bool fireEventOnce = true;
        public bool resetCounterAfterEvent = false;
        public UnityEvent Event = new UnityEvent();

        public void IncreaseCounter() => counter++;
        public void DecreaseCounter() => counter = Math.Max(0, counter - 1);
        public void ResetCounter() => counter = 0;

        private void Update()
        {
            if (counter < fireEventAtCounter)
                return;
            Event.Invoke();

            if (fireEventOnce)
                Event.RemoveAllListeners();

            if (resetCounterAfterEvent)
                counter = 0;
        }
    }
}
