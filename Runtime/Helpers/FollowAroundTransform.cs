﻿using UnityEngine;

namespace OmiLAXR.Helpers
{
    public class FollowAroundTransform : MonoBehaviour
    {
        public Transform target;

        public float yPosition = 1.0f;
        public float distance = 0.5f;
        public float degreePosition = 90.0f;

        private Vector3 GetOffsetPosition()
        {
            var rad = Mathf.Deg2Rad * degreePosition;
            var pos2D = new Vector2(Mathf.Cos(rad), Mathf.Sin(rad)) * distance;
            return new Vector3(pos2D.x, yPosition, pos2D.y);
        }

        // Update is called once per frame
        private void Update()
        {
            if (transform == null)
                return;
            var offset = GetOffsetPosition();
            transform.position = target.position + offset;
        }
    }
}
