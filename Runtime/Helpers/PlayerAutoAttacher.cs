﻿using OmiLAXR.Adapters;
using UnityEngine;

namespace OmiLAXR.Helpers
{
    public class PlayerAutoAttacher : MonoBehaviour
    {
        public Component[] attacherComponents;
        private void Start()
        {
            foreach (var component in attacherComponents)
            {
                var attacher = (IAttacher) component;
                attacher.AttachTo(Learner.Instance.Player.GetTransform());
            }
        }
    }

}