﻿using System.ComponentModel;
using UnityEngine;

namespace OmiLAXR.Helpers
{
    /// <summary>
    /// Manages attaching a prefab to a parent transform.
    /// Cleanup automatically using attach/detach hooks.
    /// </summary>
    public enum ActiveState
    {
        Active,
        Inactive,
        Inherit
    }

    /// <summary>
    /// Attaches a prefab to a parent transform.
    /// </summary>
    public class AttachGameObject : MonoBehaviour
    {
        [Tooltip("Transform to use as a future parent.")] [Description("Transform to use as a future parent.")]
        public Transform parent;

        [Tooltip("Reference to a prefab or scene object to clone.")]
        [Description("Reference to a prefab or scene object to clone.")]
        public GameObject prefab;

        [Tooltip("Determines whether the cloned object is active or not. Can also inherit from the prefab.")]
        [Description("Determines whether the cloned object is active or not. Can also inherit from the prefab.")]
        public ActiveState activeState = ActiveState.Inherit;

        [Tooltip("Local position offset of the cloned prefab.")]
        [Description("Local position offset of the cloned prefab.")]
        public Vector3 offset = Vector3.zero;

        [Tooltip("Local rotation of the cloned prefab.")] [Description("Local rotation of the cloned prefab.")]
        public Vector3 rotation = Vector3.zero;

        [Tooltip("Local scale of the cloned prefab.")] [Description("Local scale of the cloned prefab.")]
        public Vector3 scale = Vector3.one;

        private GameObject instance;

        private void OnEnable()
        {
            instance = Instantiate(prefab, parent);

            var t = instance.transform;
            t.localPosition = offset;
            t.localRotation = Quaternion.Euler(rotation);
            t.localScale = scale;

            // Adjust active state if necessary.
            if (activeState != ActiveState.Inherit)
            {
                instance.SetActive(activeState == ActiveState.Active);
            }
        }

        private void OnDisable()
        {
            Destroy(instance);
        }
    }
}