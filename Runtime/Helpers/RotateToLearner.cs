
namespace OmiLAXR.Helpers
{
    public class RotateToLearner : RotateToTransform
    {
        private void Awake()
        {
            enabled = false;

            Learner.Instance.OnLoadPlayer += player => AttachTo(player.GetTransform());
        }
    }
}
