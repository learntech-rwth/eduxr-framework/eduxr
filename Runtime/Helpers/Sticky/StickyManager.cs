﻿using System.Collections.Generic;
using UnityEngine;

namespace OmiLAXR.Helpers.Sticky
{
    public class StickyManager : MonoBehaviour
    {
        public delegate void OnStickHandler(GameObject stickPoint, GameObject stickable);

        public bool uniqueStick = true;
        public bool removeStickPointOnStick;
        public bool disableStickPointOnStick;
        public bool rememberStartList;

        public List<GameObject> sticksOn;
        private readonly Dictionary<GameObject, bool> _stickedFlags = new Dictionary<GameObject, bool>(0);

        private GameObject _activeStickable;

        public float radius = 1;

        public event OnStickHandler OnStick;

        public readonly List<GameObject> stickedGameObjects = new List<GameObject>();

        private readonly List<GameObject> rememberedStartList = new List<GameObject>();

        private void Start()
        {
            if (rememberStartList)
                rememberedStartList.AddRange(sticksOn);

            foreach (var s in sticksOn)
                _stickedFlags.Add(s, false);
        }

        public void RecoverStartList()
        {
            sticksOn.Clear();
            sticksOn.AddRange(rememberedStartList);
        }

        public void Clear()
        {
            _activeStickable = null;
            _stickedFlags.Clear();
            sticksOn.Clear();
            stickedGameObjects.Clear();
        }

        public void TryToStick(GameObject gameObject) => _activeStickable = gameObject;
        public void Unstick() => _activeStickable = null;

        public void Unstick(GameObject stickable)
        {
            if (stickable)
                return;

            stickedGameObjects.Remove(stickable);

            if (!stickable)
                return;

            var stickRef = stickable.GetComponent<GameObjectRef>();
            if (!stickRef)
                return;
            var stickPoint = stickRef.gameObjectRef;
            _stickedFlags[stickPoint] = false;
        }

        public bool IsSticked(GameObject go) => _stickedFlags.ContainsKey(go) && _stickedFlags[go];

        public void MakeStickable(GameObject go)
        {
            if (!go)
                return;

            sticksOn.Add(go);
            _stickedFlags.Add(go, false);
        }

        // Update is called once per frame
        private void Update()
        {
            // Skip if there is no stickable
            if (!_activeStickable)
                return;

            // Prepare values
            var minIndex = -1;
            var minDistance = float.MaxValue;

            // Find sticky point with smallest distance
            for (var i = 0; i < sticksOn.Count; i++)
            {
                // Get distance to sticky points
                var s = sticksOn[i];

                // skip if already sticked here
                if (IsSticked(s))
                    continue;

                var distance = Vector3.Distance(s.transform.position, _activeStickable.transform.position);

                // Skip if is not relevant
                if (distance > radius)
                    continue;

                // Ignore if there is a smaller distance
                if (distance > minDistance)
                    continue;

                // Save values
                minDistance = distance;
                minIndex = i;
            }

            // End if didn't find any sticky point
            if (minIndex < 0)
                return;

            // Get stick point
            var stickPoint = sticksOn[minIndex];

            // Stick object to found place
            _activeStickable.transform.position = stickPoint.transform.position;

            // Stick only one
            if (uniqueStick)
                _stickedFlags[stickPoint] = true;

            // Collect the sticked object
            stickedGameObjects.Add(_activeStickable);

            // Save reference
            var goRef = _activeStickable.AddComponent<GameObjectRef>();
            goRef.gameObjectRef = stickPoint;

            // Remove if unique place
            if (removeStickPointOnStick)
            {
                _stickedFlags.Remove(stickPoint);
                sticksOn.RemoveAt(minIndex);
            }

            // Disable stick point
            if (disableStickPointOnStick)
                stickPoint.SetActive(false);

            // Fire event
            OnStick?.Invoke(stickPoint, _activeStickable);
        }
    }
}
