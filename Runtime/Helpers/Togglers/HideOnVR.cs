﻿using UnityEngine;

namespace OmiLAXR.Helpers.Togglers
{
    public class HideOnVR : MonoBehaviour
    {
        private void Start()
        {
            if (Learner.Instance.IsVR)
            {
                gameObject.SetActive(false);
            }
        }
    }

}