﻿using UnityEngine;
using UnityEngine.Events;

namespace OmiLAXR.Helpers.Togglers
{
    [System.Serializable]
    public class ToggleEvent : UnityEvent<bool> { }

    public class ToggleController : MonoBehaviour
    {
        public UnityEvent onToggleOn = new UnityEvent();
        public UnityEvent onToggleOff = new UnityEvent();
        public ToggleEvent onToggle = new ToggleEvent();

        [SerializeField, Tooltip("Initial state of the toggle")]
        private bool state;

        public bool State { get => state; set => Toggle(value); }

        public void Toggle(bool newState)
        {
            state = newState;
            (state ? onToggleOn : onToggleOff).Invoke();

            onToggle.Invoke(state);
        }

        public void Toggle() => Toggle(!state);
    }
}