﻿using UnityEngine;
using UnityEngine.Serialization;

namespace OmiLAXR.Helpers.Togglers
{
    [DefaultExecutionOrder(1000)]
    public class HideOnStart : MonoBehaviour
    {
        [FormerlySerializedAs("onVR")] public bool onXR = true;
        [FormerlySerializedAs("onNonVR")] public bool onNonXR = true;
        private void Start()
        {
            Learner.Instance.OnLoadPlayer += _ => Hide();
            Hide();
        }
        
        private void Hide()
        {
            if (!Learner.Instance.IsPlayerLoaded)
                return;
            
            var isXR = Learner.Instance.IsXR;
            if ( (!onXR || !isXR) && (!onNonXR || isXR)) 
                return;
            
            DebugLog.OmiLAXR.Warning(name + " was prevented to be enabled, because it has the component 'HideOnStart'.");
            gameObject.SetActive(false);
        }
    }

}