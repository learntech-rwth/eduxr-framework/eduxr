﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace OmiLAXR.Helpers.Togglers
{
    public class StateOnOffToggler : MonoBehaviour
    {
        [Tooltip("Seconds, a state is hold.")]
        [Min(0)]
        public float holdDuration = 1.0f;

        public bool autoStart;
        public bool startWithOn;
        public bool isRunning { get; private set; }
        private float _elapsedSeconds;

        private bool _state;

        public UnityEvent onToggledOn;
        public UnityEvent onToggledOff;

        private void Start()
        {
            if (autoStart)
                isRunning = true;
            if (startWithOn)
            {
                _state = true;
                onToggledOn.Invoke();
            }
            else 
                onToggledOff.Invoke();
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (!isRunning)
                return;
            
            // count elapsed time
            _elapsedSeconds += Time.fixedDeltaTime;
            // if not holding long
            if (_elapsedSeconds < holdDuration)
                return;
            // Toggle state
            _state = !_state;
            // Fire event
            if (_state)
                onToggledOn.Invoke();
            else
                onToggledOff.Invoke();

            // Reset time
            _elapsedSeconds = 0;
        }
        public void Play()
        {
            Stop();
            Resume();
        }
        public void Resume() => isRunning = true;
        public void Pause() => isRunning = false;
        public void Stop()
        {
            isRunning = false;
            _elapsedSeconds = 0.0f;
        }
    }

}