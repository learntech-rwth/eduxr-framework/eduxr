using UnityEngine;

namespace OmiLAXR.Helpers.Togglers
{
    public class TurnPlayerOnStart : MonoBehaviour
    {
        private void Start()
        {
            Learner.Instance.OnLoadPlayer += player =>
            {
                var playerTransform = player.GetTransform();
                var dir = transform.position - playerTransform.position;
                dir.y = 0;
                playerTransform.LookAt(dir);
            };
        }
    }
}