﻿using UnityEngine;
using UnityEngine.Events;

namespace OmiLAXR.Helpers.Togglers
{
    public class OnEnableDisableController : MonoBehaviour
    {
        public UnityEvent onEnabled;
        public UnityEvent onDisabled;
        private void OnEnable() => onEnabled.Invoke();
        private void OnDisable() => onDisabled.Invoke();
    }

}