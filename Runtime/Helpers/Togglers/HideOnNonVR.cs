﻿
using UnityEngine;

namespace OmiLAXR.Helpers.Togglers
{
    [DefaultExecutionOrder(0)]
    public class HideOnNonVR : MonoBehaviour
    {
        private void Start()
        {
            if (!Learner.Instance.IsVR)
            {
                gameObject.SetActive(false);
            }
        }
    }

}