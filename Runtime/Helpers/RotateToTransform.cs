﻿using OmiLAXR.Adapters;
using UnityEngine;

namespace OmiLAXR.Helpers
{
    public class RotateToTransform : MonoBehaviour, IAttacher
    {
        public Transform attachedTo;

        public bool ignoreXAxis;
        public bool ignoreYAxis;
        public bool ignoreZAxis;

        public bool flipX;
        public bool flipY;
        public bool flipZ;

        public void AttachTo(Transform t)
        {
            attachedTo = t;
            enabled = true;
        }

        protected virtual void Start()
        {
            if (attachedTo) 
                return;
            DebugLog.OmiLAXR.Error("You need to attach a game object you want to follow!");
            enabled = false;
        }
        // Update is called once per frame
        private void Update()
        {
            // Compute direction of attached object and this transform
            var direction = (attachedTo.position - transform.position).normalized;

            // Ignore an axis
            if (ignoreXAxis)
                direction.x = 0;
            if (ignoreYAxis)
                direction.y = 0;
            if (ignoreZAxis)
                direction.z = 0;

            // Flip an axis
            if (flipX)
                direction.x *= -1;
            if (flipY)
                direction.y *= -1;
            if (flipZ)
                direction.z *= -1;

            // Rotate to direction
            var rotation = Quaternion.LookRotation(direction);
            transform.localRotation = rotation;
        }
    }
}