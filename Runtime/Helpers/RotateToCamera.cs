﻿using UnityEngine;

namespace OmiLAXR.Helpers
{
    public class RotateToCamera : RotateToTransform
    {
        // Start is called before the first frame update
        protected override void Start()
        {
            if (Camera.main is null)
                return;
            attachedTo = Camera.main.transform;
        }
    }
}