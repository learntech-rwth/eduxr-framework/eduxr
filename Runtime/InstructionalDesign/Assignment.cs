﻿namespace OmiLAXR.InstructionalDesign
{
    public class Assignment : LearningStep<LearningTask>
    {
        public LearningTask GetActiveTask() => (LearningTask)GetActiveStep();
        public LearningTask GetTask(string id) => (LearningTask)GetStep(id);
    }
}