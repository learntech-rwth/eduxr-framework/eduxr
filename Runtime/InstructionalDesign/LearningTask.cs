﻿namespace OmiLAXR.InstructionalDesign
{
    public class LearningTask : LearningStep<LearningTask>
    {
        public LearningTask GetActiveSubTask() => (LearningTask)GetActiveStep();
        public LearningTask GetSubTask(string id) => (LearningTask)GetStep(id);
    }
}