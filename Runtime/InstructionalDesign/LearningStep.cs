﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.Events;

namespace OmiLAXR.InstructionalDesign
{
    public abstract class LearningStep<TC> : MonoBehaviour, ILearningStep
        where TC : MonoBehaviour, ILearningStep
    {
        public string identifier;

        [Tooltip("If empty it will assign itself.")]
        public GameObject stepsHolder;
        private TC[] _steps;
        private readonly List<ILearningStep> _todoList = new List<ILearningStep>();

        public bool endsAfterComplete = true;

        public int AmountOfRuns { get; private set; }
        public int AmountOfCompletes { get; private set; }

        [TextArea]
        public string description;
        public event Action<ILearningStep, ILearningStep> Began;
        public event Action<ILearningStep, ILearningStep> Completed;
        public event Action<ILearningStep, ILearningStep> Ended; 
        public event Action<ILearningStep, ILearningStep, ILearningStep> ProgressChanged;
        public event Action<ILearningStep> Loaded;
        
        public UnityEvent onBegin = new UnityEvent();
        public UnityEvent onEnd = new UnityEvent();
        public UnityEvent onComplete = new UnityEvent();
        
        public ILearningStep Parent { get; set; }

        public bool IsRoot => Parent == null || Parent.GetType() == GetType();
        public bool IsLeaf => _steps == null || _steps.Length == 0;
        public string GetTypeName() => GetType().Name;
        public string GetName() => name;
        public TC[] GetSteps() => _steps;
        public string[] GetStepNames() => _steps.Select(s => s.GetName()).ToArray();
        public string GetIdentifier() => identifier;

        public bool MatchesIdentifier(string otherIdentifier)
            => string.Equals(otherIdentifier, identifier, StringComparison.OrdinalIgnoreCase);
        public string GetDescription() => string.Empty == description ? name : description;
        public string GetContextName() => string.Empty == description ? (identifier == string.Empty ? name : identifier) : description;

        public bool IsCompleted => _todoList.Count == 0;
        public int StepsDone { get; private set; }
        public int StepsToDo => _steps.Length - StepsDone;
        public int StepsTotal => _steps.Length;

        private void Awake()
        {
            if (!stepsHolder)
                stepsHolder = gameObject;

            if (IsLeaf)
            {
                // if is leaf, then look for possible childs
                _steps = stepsHolder.GetComponentsInChildren(typeof(TC), includeInactive: true)
                    .Where(tc => tc.GetType() != GetType())
                    .Select(c => (TC)c).ToArray();
            }

            ResetTodoList();

            if (identifier == string.Empty)
                identifier = name.ToLower();

            // set parent for each step
            foreach (var i in _steps)
                i.Parent = this;
            // listen if sth. happen in childs and give it to parents
            foreach (var step in _steps)
            {
                step.Began += (sender, startedStep) => Began?.Invoke(this, startedStep);
                step.Ended += (sender, endedStep) => DoNextStep(endedStep);
            }
        }

        public ILearningStep CurrentStep { get; set; }
        public bool IsCurrentStep
        {
            get
            {
                var node = (ILearningStep)LearningScenario.Instance;
                while (!node.Equals(this))
                {
                    if (node.IsLeaf)
                        return false;
                    node = node.CurrentStep;
                }

                return true;
            }
        }

        protected virtual void Start()
        {
            if (IsLeaf)
                Loaded?.Invoke(this);
        }
        
        public void DoneStep(string stepIdentifier)
        {
            var activeStep = GetActiveStep();
            if (activeStep == null)
                return;
            
            // check if the step is asked, now
            if (!activeStep.MatchesIdentifier(stepIdentifier))
            {
                DebugLog.OmiLAXR.Warning($"'{stepIdentifier}' is not the current step.");
                return;
            }
            // look for step in queue
            var step = _todoList.Find(tc => tc.MatchesIdentifier(stepIdentifier));
            // check if step is null (already done, because not in todo list)
            if (step == null)
            {
                DebugLog.OmiLAXR.Warning($"Step '{stepIdentifier}' was already done.");
                return;
            }
            
            DoNextStep(step);
        }

        //  Take next step and mark it as done
        public void DoneStep() => DoNextStep(GetActiveStep());

        public void DoNextStep(ILearningStep endedStep)
        {
            print($"[OmiLAXR]: Done step in {GetTypeName()} '{GetDescription()}'.");

            if (StepsToDo < 2)
            {
                Complete();
                return;
            }

            var index = _todoList.IndexOf(endedStep);
            if (index < 0)
            {
                DebugLog.OmiLAXR.Warning($"'{endedStep.GetName()}' does not exists in todo list.");
                return;
            }

            var nextStep = _todoList[index + 1];
            // mark current as done
            if (endsAfterComplete)
            {
                _todoList.Remove(endedStep);
                StepsDone++;
            }

            nextStep.Begin();
        }

        public bool HasItems => _steps != null && _steps.Length > 0;
        public ILearningStep GetActiveStep() => _todoList.Count > 0 ? CurrentStep : null;
        public ILearningStep GetStep(string id) => _steps.First(s => s.GetIdentifier() == id);
        public TC GetFirstStep() => HasItems ? _steps[0] : null;

        public string GetDeepDescription() 
        {
            var desc = $"the {GetTypeName()} '{GetDescription()}'";

            if (IsRoot)
                return desc;
            
            var parentDescription = Parent != null ? Parent.GetDeepDescription() : "";

            return $"{desc} from {parentDescription}";
        }

        public void Restart()
        {
            print($"[OmiLAXR]: Restarting {GetDescription()}.");
            Begin();
        }
        public void Begin()
        {
            // stop active step
            GetActiveStep()?.End();
            
            ResetTodoList();
            AmountOfRuns++;
            
            // let parent know which is the current step
            if (Parent != null)
                Parent.CurrentStep = this;

            // if is not leaf, than go deeper
            if (!IsLeaf)
            {
                // choose first one
                GetFirstStep().Begin();
                return;
            }
            
            print($"[OmiLAXR]: New step is {GetDeepDescription()}.");
            // reached the leaf node
            Began?.Invoke(this, this);
            onBegin?.Invoke();
        }

        public void BeginStep(string stepIdentifier)
        {
            var step = _steps.First(s => s.MatchesIdentifier(stepIdentifier));
            if (!step)
            {
                DebugLog.OmiLAXR.Warning($"'{stepIdentifier}' is does not exist.");
                return;
            }
            step.Begin();
        }

        public void EndStep(string stepIdentifier)
        {
            var step = _steps.First(s => s.MatchesIdentifier(stepIdentifier));
            if (!step)
            {
                DebugLog.OmiLAXR.Warning($"'{stepIdentifier}' is does not exist.");
                return;
            }
            step.End();
        }
        
        public void End()
        {
            _todoList.Clear();
            Ended?.Invoke(this, this);
            onEnd?.Invoke();
        }

        public void Complete()
        {
            if (!IsCurrentStep)
            {
                //print($"[OmiLAXR]: Completed {GetTypeName()} is not the current step.");
                return;
            }
            print($"[OmiLAXR]: Completed {GetTypeName()} '{GetDescription()}'.");
            Completed?.Invoke(this, this);
            onComplete?.Invoke();
            StepsDone++;
            AmountOfCompletes++;

            if (endsAfterComplete)
                End();
        }
        
        private void ResetTodoList()
        {
            _todoList.Clear();
            foreach (var step in _steps)
            {
                _todoList.Add(step);
            }

            StepsDone = 0;
        }

    }
}