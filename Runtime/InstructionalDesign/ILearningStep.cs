﻿using System;

namespace OmiLAXR.InstructionalDesign
{
    public interface ILearningStep
    {
        // progress
        void Restart();
        void Begin();
        void End();
        void Complete();
        void DoneStep();
        void DoneStep(string stepIdentifier);
        // information
        string GetDescription();
        string GetDeepDescription();
        string GetName();
        string GetIdentifier();
        ILearningStep GetActiveStep();
        bool MatchesIdentifier(string identifier);
        string GetTypeName();
        // events
        // <sender, step>
        event Action<ILearningStep,ILearningStep> Began;
        // <sender, step>
        event Action<ILearningStep,ILearningStep> Ended;
        // <sender, step>
        event Action<ILearningStep,ILearningStep> Completed;
        // <sender, oldStep, newStep>
        event Action<ILearningStep, ILearningStep, ILearningStep> ProgressChanged;
        event Action<ILearningStep> Loaded;
        // fields
        bool IsCompleted { get; }
        bool IsRoot { get; }
        bool IsLeaf { get; }
        int StepsDone { get; }
        int StepsToDo { get; }
        int StepsTotal { get; }
        ILearningStep Parent { get; set; }
        ILearningStep CurrentStep { get; set; }
    }
}