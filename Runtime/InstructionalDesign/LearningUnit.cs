﻿using UnityEngine;

namespace OmiLAXR.InstructionalDesign
{
    [DisallowMultipleComponent]
    public class LearningUnit : LearningStep<Assignment>
    {
        public Assignment GetActiveAssignment() => (Assignment)GetActiveStep();
        public Assignment GetAssignment(string id) => (Assignment)GetStep(id);
    }
}