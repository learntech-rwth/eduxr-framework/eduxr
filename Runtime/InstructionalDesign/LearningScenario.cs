using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace OmiLAXR.InstructionalDesign
{
    [DisallowMultipleComponent]
    public class LearningScenario : LearningStep<LearningUnit>
    {
        private static LearningScenario _instance;
        /// <summary>
        /// Singleton instance of the LearningScenario. Only one can exist at a time.
        /// </summary>
        public static LearningScenario Instance
            => _instance ??= FindObjectOfType<LearningScenario>(true);
        

        public LearningUnit startingLearningUnit;

        public Action OnStarted;

        public string Scene { get; private set; }

        protected override void Start()
        {
            Loaded += OnLoaded;
            base.Start();

            Scene = SceneManager.GetActiveScene().name;

            // if no lu is defined, than use the first one
            if (!startingLearningUnit)
                startingLearningUnit = GetFirstStep();

            if (startingLearningUnit)
            {
                // start
                startingLearningUnit.Begin();    
            }
            
            OnStarted?.Invoke();
        }

        private void OnLoaded(ILearningStep sender)
        {
            print($"[LearningScenario] Found following learning units: " + string.Join(",", GetStepNames()));
        }

        public LearningUnit GetActiveLearningUnit() => (LearningUnit)GetActiveStep();

        public LearningUnit GetLearningUnit(string id) => (LearningUnit)GetStep(id);
        public static string GetGameName() => Application.productName;
        public static string GetVersion() => Application.version;
    }
}