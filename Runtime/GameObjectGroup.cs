﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace OmiLAXR
{
    public class GameObjectGroup : List<GameObject>
    {
        private readonly string[] _groupItems;
        public GameObjectGroup(params string[] gameObjectNames)
        {
            _groupItems = gameObjectNames;
            FindReferences(gameObjectNames);
        }

        public GameObjectGroup(params GameObject[] gameObjects)
        {
            _groupItems = gameObjects.Select(go => go.name).ToArray();
            AddRange(gameObjects);
        }

        public GameObjectGroup FindReferences(params string[] names)
        {
            if (names.Length == 0)
                names = _groupItems;
            Clear();
            foreach (var name in names)
            {
                var go = GameObject.Find(name);
                Add(go);
            }

            return this;
        }

        public GameObjectGroup ActionFor(Action<GameObject> action, params string[] names)
        {
            var matchedGos = this.Where(go => Array.IndexOf(names, go.name) > -1);
            foreach (var go in matchedGos)
                action(go);
            return this;
        }

        public GameObjectGroup ActionForAll(Action<GameObject> action) => ActionFor(action, _groupItems);

        public static GameObjectGroup ActionFor(Action<GameObject> action, params GameObject[] gameObjects)
        {
            var group = new GameObjectGroup(gameObjects);
            return group.ActionForAll(action);
        }
    }
}