﻿using System;
using UnityEngine;

namespace OmiLAXR
{
    /// <summary>
    /// Adds a simple way to trigger an event when the associated
    /// game object is destroyed.
    /// </summary>
    public class DestructionListener : MonoBehaviour
    {
        /// <summary>
        /// Triggered when the game object is removed. 
        /// Supplies the now-destroyed game object as parameter.
        /// </summary>
        public event Action<GameObject> Destroyed;

        private void OnDestroy()
        {
            Destroyed?.Invoke(gameObject);
        }
    }
}