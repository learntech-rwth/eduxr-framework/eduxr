﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace OmiLAXR
{
    public abstract class SingletonBehaviour : MonoBehaviour
    {
        /// <summary>
        /// Setting which destroys singletons on Awake
        /// </summary>
        [Obsolete("Experimental")]
        public static bool destroySingletonsOnAwake = true;
        /// <summary>
        /// Get triggered if object get destroyed, because there already exists one.
        /// </summary>
        public event Action<Type> OnDestroyed;
        /// <summary>
        /// GameObject of singleton instance.
        /// </summary>
        /// <returns></returns>
        public GameObject GetInstanceGameObject() => GetInstance().gameObject;
        /// <summary>
        /// Abstract representation of the singleton instance.
        /// </summary>
        /// <returns></returns>
        public abstract MonoBehaviour GetInstance();

        protected virtual void Awake()
        {
            if (!destroySingletonsOnAwake) 
                return;
            
            var instance = GetInstance();
            if (GetInstanceID() != instance.GetInstanceID())
                DestroyImmediate(gameObject);
        }

        private void OnDestroy() => OnDestroyed?.Invoke(GetType());
    }
    [Obsolete("Performance Warning! Don't use it.", true)]
    internal static class Singleton
    {
        private static readonly Dictionary<Type, Object> Singletons = new Dictionary<Type, Object>();

        [Obsolete("Performance Warning. Will be dropped soon.", true)]
        public static T Get<T>() where T : Object
        {
            var type = typeof(T);
            if (Singletons.ContainsKey(type))
                return (T)Singletons[type];
            var instance = Object.FindObjectOfType<T>();
            Singletons.Add(type, instance);
            return instance;
        }

        public static T Get<T>(T instance) where T : Object
            => instance == null ? instance = Object.FindObjectOfType<T>() : instance;
    }
}