﻿using UnityEngine;

namespace OmiLAXR
{
    public class ExceptionThrower : MonoBehaviour
    {
        public class MessagedException : System.Exception 
        { 
            public MessagedException(string message) : base(message)
            {

            }
        }

        public string message = "No message";
        public bool throwOnStart = false;

        private void Start()
        {
            if (throwOnStart)
                ThrowException();
        }

        public void ThrowException() => throw new MessagedException(message);
    }

}