﻿using System;
using UnityEngine;

namespace OmiLAXR.Effects
{
    public class BlinkController : MonoBehaviour
    {
        [Tooltip("The color for the blink.")]
        public Color blinkColor = Color.white;
        [Tooltip("Seconds, how long the blink color should hold on.")]
        [Min(0)]
        public float holdDuration = 1.0f;
        [Tooltip("Duration how long a fade animation should hold.")]
        [Min(0)]
        public float fadeDuration = 0.5f;
        /// <summary>
        /// Current state of the blink.
        /// </summary>
        public BlinkState state { get; private set;  }
        /// <summary>
        /// True, if blinking is in action.
        /// </summary>
        public bool isRunning { get; private set; }

        private Color defaultColor;
        private float elapsedSeconds = 0.0f;

        private Color nextColor = Color.white;

        public virtual Color GetColor() => throw new NotImplementedException();
        public virtual void SetColor(Color color) => throw new NotImplementedException();
        public void RestoreColor() => SetColor(defaultColor);

        protected virtual void Start()
        {
            defaultColor = GetColor();
            Hold(BlinkState.Idle);
            isRunning = true;
        }

        private void FixedUpdate()
        {
            if (!isRunning)
                return;
            // count time
            elapsedSeconds += Time.fixedDeltaTime;
            // get percentage
            var t = elapsedSeconds / fadeDuration;
            // interpolate the color
            SetColor(Color.Lerp(GetColor(), nextColor, t));

            if (state == BlinkState.Blink || state == BlinkState.Idle)
            {
                if (elapsedSeconds < holdDuration)
                    return;
                // if time is reached, change blink state
                if (state == BlinkState.Blink)
                    FadeOut();
                else if (state == BlinkState.Idle)
                    FadeIn();
                return;
            }

            if (elapsedSeconds < fadeDuration)
                return;

            // If animation is finished
            if (state == BlinkState.FadeIn)
                Hold(BlinkState.Blink);
            else if (state == BlinkState.FadeOut)
                Hold(BlinkState.Idle);
        }

        public void Hold(BlinkState state)
        {
            elapsedSeconds = 0f;
            this.state = state;
        }
        public void FadeIn()
        {
            elapsedSeconds = 0f;
            state = BlinkState.FadeIn;
            nextColor = blinkColor;
        }
        public void FadeOut()
        {
            elapsedSeconds = 0f;
            state = BlinkState.FadeOut;
            nextColor = defaultColor;
        }
        public void Play()
        {
            Stop();
            Resume();
        }
        public void Resume() => isRunning = true;
        public void Pause() => isRunning = false;
        public void Stop()
        {
            isRunning = false;
            state = BlinkState.Idle;
            elapsedSeconds = 0.0f;
            SetColor(defaultColor);
        }
    }
}
