﻿using UnityEngine;
using UnityEngine.UI;

namespace OmiLAXR.Effects
{
    // Adapt Blink Controller for Image element.
    [RequireComponent(typeof(Image))]
    public class BlinkImageController : BlinkController
    {
        private Image _image;
     
        protected override void Start()
        {
            _image = GetComponent<Image>();
            base.Start();
        }

        public override Color GetColor() => _image.color;
        public override void SetColor(Color color)
        {
            if (_image == null)
                return;

            _image.color = color;
        }
    }

}