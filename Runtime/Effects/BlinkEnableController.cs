﻿using System.Linq;
using UnityEngine;

namespace OmiLAXR.Effects {
    public class BlinkEnableController : MonoBehaviour
    {
        [Tooltip("Seconds, how long the blink color should hold on.")]
        [Min(0)]
        public float holdDuration = 1.0f;
        /// <summary>
        /// Current state of the blink.
        /// </summary>
        public BlinkState State { get; private set;  }
        /// <summary>
        /// True, if blinking is in action.
        /// </summary>
        public bool IsRunning { get; private set; }
        private float _elapsedSeconds = 0.0f;

        private MonoBehaviour _component;

        private void Start()
        {
            var components = GetComponents<MonoBehaviour>().Where(c => c.GetType() != GetType());
            _component = components.Last();
            IsRunning = true;
        }

        public void Play() => IsRunning = true;
        public void Pause() => IsRunning = false;

        public void Stop()
        {
            _elapsedSeconds = 0;
            ChangeState(BlinkState.FadeOut);
            IsRunning = false;
        }

        private void FixedUpdate()
        {
            if (!IsRunning)
                return;
            // count time
            _elapsedSeconds += Time.fixedDeltaTime;

            if (!(_elapsedSeconds >= holdDuration)) return;
            ChangeState(State == BlinkState.FadeIn ? BlinkState.FadeOut : BlinkState.FadeIn);
            _elapsedSeconds = 0;
        }

        private void ChangeState(BlinkState state)
        {
            this.State = state;
            _component.enabled = state == BlinkState.FadeIn;
        }
    }

}