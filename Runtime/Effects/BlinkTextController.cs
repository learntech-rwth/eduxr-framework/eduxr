﻿using TMPro;
using UnityEngine;

namespace OmiLAXR.Effects
{
    // Adapt blinking animation vor TextMeshProUGUI
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class BlinkTextController : BlinkController
    {
        private TextMeshProUGUI _textMesh;
        protected override void Start()
        {
            _textMesh = GetComponent<TextMeshProUGUI>();
            base.Start();
        }
        public override Color GetColor() => _textMesh.color;
        public override void SetColor(Color color) => _textMesh.color = color;
    }
}