﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OmiLAXR.Effects
{
    public enum BlinkState
    {
        FadeIn,
        FadeOut,
        Blink,
        Idle
    }
}