using System;
using UnityEngine;
using OmiLAXR.Timeline.MultiLanguage;

namespace OmiLAXR
{
    [DisallowMultipleComponent]
    [AddComponentMenu("OmiLAXR / Learning Environment")]
    public class LearningEnvironment : SingletonBehaviour
    {
        public MultiLanguageLanguages language = MultiLanguageLanguages.English;
        private static LearningEnvironment _instance;
        /// <summary>
        /// Singleton instance of the LearningScenario. Only one can exist at a time.
        /// </summary>
        public static LearningEnvironment Instance
        {
            get
            {
                if ( _instance == null )
                {
                    _instance = FindObjectOfType<LearningEnvironment>();
                    if (_instance == null)
                    {
                        // if is still null
                        DebugLog.OmiLAXR.Error("Cannot find LearningEnvironment instance. Make sure you have added a 'LearningEnvironment' component to a game object.");
                    }
                }
                return _instance;
            }
        }

        public static bool HasInstance => _instance != null;
        public event Action<MultiLanguageLanguages> OnChangedLanguage;
        public event Action<string> OnReceiveNewInstruction;
        public override MonoBehaviour GetInstance() => Instance;
        
        public void SetLanguage(MultiLanguageLanguages lang)
        {
            language = lang;
            OnChangedLanguage?.Invoke(lang);
            PlayerPrefs.SetString("language", lang.ToString());
        }

        public void SetLanguage(string language)
        {
            if (Enum.TryParse(language, true, out MultiLanguageLanguages lang))
            {
                SetLanguage(lang);
            }
        }

        public void ProvideNewInstruction(string text)
        {
            OnReceiveNewInstruction?.Invoke(text);
        }
        
        public Transform GetTransformFromRole(Roles role)
        {
            if (role == Roles.Guide)
                return Guide.Instance.transform;
            if (role == Roles.Learner)
                return Learner.Instance.transform;
            // todo: collaborators
            throw new NotSupportedException("The role " + role + " is not supported for this function.");
        }

    }
}
