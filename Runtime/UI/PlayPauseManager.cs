﻿using OmiLAXR.Timeline;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

namespace OmiLAXR.UI
{
    [RequireComponent(typeof(Button), typeof(PlayPauseDirector))]
    public class PlayPauseManager : MonoBehaviour
    {
        public PlayPauseDirector playPauseDirector;
        public Button button;

        private bool _isDownSpace;
        private bool _isDownP;

        public bool IsIgnoringPause => _isDownP && _isDownSpace;
        
        private void Start()
        {
            playPauseDirector = GetComponent<PlayPauseDirector>();
            button = GetComponent<Button>();
        }

        public void Pause()
        {
            if (IsIgnoringPause)
                return;
            playPauseDirector.Pause();
        }

        public void Run(PlayableDirector activeDirector)
        {
            if (playPauseDirector.director)
                playPauseDirector.director.enabled = false;

            playPauseDirector.director = activeDirector;
            activeDirector.enabled = true;
            
            playPauseDirector.enabled = true;
            button.gameObject.SetActive(true);
            
            playPauseDirector.director.Play();
        }

        public void Stop()
        {
            if (playPauseDirector)
            {
                playPauseDirector.enabled = false;
                playPauseDirector.IsNotWaitingAnymore();

                if (playPauseDirector.director)
                {
                    playPauseDirector.director.Stop();
                    playPauseDirector.director.enabled = false;
                }
            }
            button.gameObject.SetActive(false);
        }

        private void Update()
        {
            _isDownSpace = Input.GetKeyDown(KeyCode.Space);
            _isDownP = Input.GetKeyDown(KeyCode.P);
        }
    }
}