﻿using UnityEngine;

namespace OmiLAXR.UI
{
    public delegate void OmiLAXRUiElementChangeHandler<in T>(MonoBehaviour sender, T value);
}