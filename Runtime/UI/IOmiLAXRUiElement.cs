﻿using UnityEngine;

namespace OmiLAXR.UI
{
    public interface IOmiLAXRUiElement<out T>
    {
        event OmiLAXRUiElementChangeHandler<T> OnChanged;
    }
}