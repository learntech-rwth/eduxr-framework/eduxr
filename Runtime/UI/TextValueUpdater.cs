﻿using UnityEngine;
using TMPro;

namespace OmiLAXR.UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TextValueUpdater : MonoBehaviour
    {
        public string textFormat = "{0:0.00}";
        public TextMeshProUGUI textMesh;
        public float multiplier = 1f;
        // Start is called before the first frame update
        void Start()
        {
            if (textMesh == null)
                textMesh = GetComponent<TextMeshProUGUI>();
        }

        public void UpdateText(float value)
        {
            textMesh.text = string.Format(textFormat, value * multiplier);
        }
    }
}