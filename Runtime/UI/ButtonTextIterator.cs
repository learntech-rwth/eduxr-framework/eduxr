﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using System.Collections.Generic;

namespace OmiLAXR.UI
{
    [RequireComponent(typeof(Button))]
    public class ButtonTextIterator : MonoBehaviour
    {
        public TextMeshProUGUI textMesh;
        public Button button;
        public List<string> texts;
        public event Action<string> OnNextText;
        public bool autoReactOnButton = true;

        private void Start()
        {
            button = GetComponent<Button>();
            if (textMesh == null)
                textMesh = GetComponentInChildren<TextMeshProUGUI>();

            if (autoReactOnButton)
                button.onClick.AddListener(UpdateText);
            
            UpdateText();
        }

        public void UpdateText()
        {
            // avoid divide through 0
            if (texts.Count < 1)
                return;
            var text = textMesh.text;
            // get current text
            var index = texts.IndexOf(text);
            var nextIndex = (index + 1) % texts.Count;
            var nextText = texts[nextIndex];
            textMesh.text = nextText;
            OnNextText?.Invoke(nextText);
        }
    }

}