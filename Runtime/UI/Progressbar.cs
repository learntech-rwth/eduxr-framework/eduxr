﻿using UnityEngine;
using UnityEngine.Playables;

namespace OmiLAXR.UI
{
    public class Progressbar : MonoBehaviour
    {
        public PlayableDirector director;
        private GameObject progressGo;

        private void Start()
        {
            progressGo = transform.Find("Over").gameObject;
        }

        public void SetDirector(PlayableDirector director) => this.director = director;

        private void Update()
        {
            if (!director)
                return;
            var duration = director.duration;
            var cur = director.time;
            // Avoid division by 0
            if (duration <= 0)
                return;
            // Compute progress in percent (between 0 and 1)
            var progress = cur / duration;
            // Apply progress to bar scale
            var s = progressGo.transform.localScale;
            s.x = (float) progress;
            progressGo.transform.localScale = s;
        }
    }

}