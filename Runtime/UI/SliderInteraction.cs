﻿using OmiLAXR.Interaction;
using UnityEngine;

using UnityEngine.UI;

namespace OmiLAXR.UI
{
    public class SliderInteraction : MonoBehaviour
    {
        private MenuPanel _menuPanel;

        private const float ClampX = -44.2f;

        private bool _isDragging;

        public float Percentage { get; private set; }
        private int _tempPerc;

        public Slider.SliderEvent OnValueChanged = new Slider.SliderEvent();
        
        public float minValue;
        public float maxValue = 100;
        public float rawValue;
        private float _startValue;

        [Header("Control elements")] 
        public RectTransform over;
        public Pointable point;
        public RectTransform start;

        // Start is called before the first frame update
        protected void Start()
        {
            _startValue = rawValue;
            if (point)
            {
                point.onPressed.AddListener(e => _isDragging = true);
                point.onReleased.AddListener(e => _isDragging = false);
            }
            
            _menuPanel = GetComponentInParent<MenuPanel>();

            DisablePointColliderEvent();
        }

        private void DisablePointColliderEvent()
        {
            var sphereCollider = GetComponentInChildren<SphereCollider>();
            if (!sphereCollider)
                return;
            var pointable = sphereCollider.gameObject.GetComponent<Pointable>();
            if (!pointable)
                return;
            pointable.onSelect.AddListener(_ => sphereCollider.enabled = false);
            pointable.onUnselect.AddListener(_ => sphereCollider.enabled = true);
        }

        public void ResetValue() => rawValue = _startValue;

        public void SetPercentage(float percentage) => SetValue(percentage * ClampX);
        
        public void SetValue(float v)
        {
            // catch zero division
            if (maxValue - minValue < 0.000001f)
            {
                maxValue += 1.0f;
            }

            // Clamp values
            v = Mathf.Clamp(v, minValue, maxValue);
            rawValue = v;
            Percentage = (v - minValue) / (maxValue - minValue);
            
            OnValueChanged?.Invoke(v);
            
            // Scale over bar
            over.localScale = new Vector3(Percentage, 1, 1);

            // Set point to correct position
            point.transform.position = start.position + new Vector3(Percentage, 0, -40.49697f);
            
            print("SliderInteraction " + name + " " + v);
        }

        // Update is called once per frame
        private void Update()
        {
            if (!_isDragging)
                return;

            // Transform hit point into relative position
            var pos = start.worldToLocalMatrix.MultiplyPoint(_menuPanel.HitPoint);
            SetValue(pos.x);
        }
    }
}
