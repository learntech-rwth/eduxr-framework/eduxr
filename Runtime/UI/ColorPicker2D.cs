﻿using UnityEngine;
using UnityEngine.EventSystems;

using Image = UnityEngine.UI.Image;

using System;
using OmiLAXR.Extensions;

namespace OmiLAXR.UI
{
    [RequireComponent(typeof(Image))]
    public class ColorPicker2D : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerEnterHandler, IOmiLAXRUiElement<Color>
    {
        public GameObject picker;
        private RectTransform _pickerRect;

        [Tooltip("Color palette texture on which the color picking should be applied.")]
        private RectTransform _rectTransform;
        private Image _image;
        private bool _isDown;
        private bool _isInsideTexture;

        public bool isPointerInteractingWith => _isDown || _isInsideTexture;
        public bool hoveredOver => _isInsideTexture;

        private Texture2D Texture => _image.sprite.texture;
        
        private Coroutine _timeout;

        private Color _activeColor;
        private Color _savedColor;

        private float _timeElapsedAfterLastChange = 0.0f;
        
        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            _isDown = true;
            OnStartInteraction?.Invoke();
        }

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData) => _isInsideTexture = true;

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData) 
        {
            _isInsideTexture = false;
            if (!_isDown)
                OnEndInteraction?.Invoke();
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            _isDown = false;
            if (!_isInsideTexture)
                OnEndInteraction?.Invoke();
        }

        public event Action OnStartInteraction;
        public event Action OnEndInteraction;
        public event OmiLAXRUiElementChangeHandler<Color> OnChanged;
        // Start is called before the first frame update
        private void Start()
        {
            _rectTransform = GetComponent<RectTransform>();
            _image = GetComponent<Image>();
            
            if (picker)
                _pickerRect = picker.GetComponent<RectTransform>();
        }

        /// <summary>
        /// Picks a color from palette by (x,y) vector.
        /// </summary>
        /// <param name="v">(x,y) texture float position of palette between 0.0f and 1.0f.</param>
        /// <returns>Color from palette</returns>
        public Color PickColor(Vector2 v) => PickColor(v.x, v.y);
        /// <summary>
        /// Picks a color from palette by (x,y) floats.
        /// </summary>
        /// <param name="x">x texture coordinate of palette between 0.0f and 1.0f.</param>
        /// <param name="y">y texture coordinate of palette between 0.0f and 1.0f.</param>
        /// <returns>Color from palette</returns>
        public Color PickColor(float x, float y) => Texture.PickColor(x, y);

        private void UpdatePickerPosition(Vector3 p)
        {
            if (_pickerRect == null)
                return;
            _pickerRect.localPosition = p;
        }

        // Update is called once per frame
        private void Update()
        {
            if (!_isDown || !_isInsideTexture)
            {
                // check if nothing has changed for 500ms    
                _timeElapsedAfterLastChange += Time.deltaTime;

                if (!(_timeElapsedAfterLastChange >= 0.5f) || _activeColor == _savedColor) 
                    return;
                
                OnChanged?.Invoke(this, _activeColor);
                _savedColor = _activeColor;

                return;
            }
            // declare texture size
            var size = _rectTransform.rect.width; // width == height
            var radius = size / 2;
            // get screen position
            var mousePos = Input.mousePosition;
            // transform position relative to gradient
            var relMousePos = _rectTransform.InverseTransformPoint(mousePos.x, mousePos.y, 0);
            // compute magnitude
            var magnitude = Vector3.Magnitude(relMousePos - new Vector3(radius, radius, 0));
            // if is outside the circle, then skip
            if (magnitude > radius)
                return;
            // map mouse pos to (0, 0) and (1, 1)
            var normalizedMousePos = new Vector2(relMousePos.x / size, relMousePos.y / size);
            // map mouse pos to (-1, 1) and (1, 1)
            var p = normalizedMousePos * 2 - Vector2.one;
            // clamp position
            p.x = Mathf.Clamp(p.x, -1, 1);
            p.y = Mathf.Clamp(p.y, -1, 1);
            // provide color
            _activeColor = PickColor(p);
            UpdatePickerPosition(relMousePos);
            _timeElapsedAfterLastChange = 0.0f;
        }

        
    }

}