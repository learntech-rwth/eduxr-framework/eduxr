﻿using OmiLAXR.Interaction;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace OmiLAXR.UI
{
    [RequireComponent(typeof(PointableUI))]
    public class MenuPanel : MonoBehaviour
    {
        [FormerlySerializedAs("OnEnterGroup")] public UnityEvent onEnterGroup = new UnityEvent();
        [FormerlySerializedAs("OnLeaveGroup")] public UnityEvent onLeaveGroup = new UnityEvent();

        public Vector3 HitPoint { get; private set; }

        protected PointableUI Pointable;

        public bool isEntered = false;
        // Start is called before the first frame update
        protected virtual void Start()
        {
            Pointable = GetComponent<PointableUI>();

            LaserPointerHandler.Instance.OnPoint += OnPoint;
            LaserPointerHandler.Instance.OnNotPoint += OnNotPoint;
        }

        private void OnHit(RaycastHit hit)
        {
            if (hit.collider.transform != transform)
                return;
            HitPoint = hit.point;
        }

        private void Leave()
        {
            if (!isEntered)
                return;

            isEntered = false;
            onLeaveGroup.Invoke();
            LaserPointerHandler.Instance.LeaveAnyPanel(gameObject);
        }

        private void Enter()
        {
            if (isEntered)
                return;
            isEntered = true;
            onEnterGroup.Invoke();
            LaserPointerHandler.Instance.EnterAnyPanel(gameObject);
        }

        private void OnNotPoint() => Leave();

        private void OnPoint(RaycastHit hit)
        {
            if (!hit.transform)
            {
                Leave();
                return;
            }
            var go = hit.collider.gameObject;
            var pointableUI = go.GetComponent<PointableUI>();
            
            if (!pointableUI || pointableUI.pointableGroup != Pointable.pointableGroup)
            {
                Leave();
                return;
            }

            Enter();
        }
    }
}
