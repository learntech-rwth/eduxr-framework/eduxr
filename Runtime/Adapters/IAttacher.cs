﻿using UnityEngine;

namespace OmiLAXR.Adapters
{
    public interface IAttacher
    {
        void AttachTo(Transform transform);
    }
}