﻿namespace OmiLAXR.Adapters
{
    public interface IAdapterSystem<in T> where T : IAdapter
    {
        public void ApplyAdapter(T adapter);
    }
}