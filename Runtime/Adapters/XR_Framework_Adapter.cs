﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace OmiLAXR.Adapters
{
    [DefaultExecutionOrder(-1000)]
    public abstract class XR_Framework_Adapter : SingletonBehaviour
    {
        /// <summary>
        /// Field holding the player prefab.
        /// </summary>
        public GameObject playerPrefab;
        /// <summary>
        /// Reference to player GameObject.
        /// </summary>
        public GameObject playerReference;
        /// <summary>
        /// Quick access to Learner reference.
        /// </summary>
        protected Learner Learner => Learner.Instance;

#if UNITY_EDITOR
        /// <summary>
        /// A function for accessing Prefab by path.
        /// </summary>
        /// <param name="path">Path of asset</param>
        /// <returns>Reference to a prefab.</returns>
        [Obsolete("Don't use this function, it is editor only.", true)]
        protected static GameObject FetchAsset(string path) => AssetDatabase.LoadAssetAtPath<GameObject>(path);
#endif
        
        /// <summary>
        /// Needed function which is loading the player.
        /// </summary>
        /// <returns>Reference to the Player GameObject.</returns>
        protected abstract GameObject LoadPlayer();
        /// <summary>
        /// Needed function which is telling the current VR mode.
        /// </summary>
        /// <returns>True, if VR is enabled.</returns>
        protected abstract bool IsVR();
        /// <summary>
        /// State which tells the framework that this adapter is still loading.
        /// </summary>
        /// <returns>True, if is loading.</returns>
        public abstract bool IsLoading();
    }
}