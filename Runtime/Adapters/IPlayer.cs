﻿using OmiLAXR.Interaction;
using UnityEngine;

namespace OmiLAXR.Adapters
{
    public interface IPlayer
    {
        public Vector3 WorldPosition { get; }
        public Vector3 WorldRotation { get; }
        public Transform GetTransform();
        public Transform GetHead();
        public Transform GetBody();
        public ILaserPointer GetLeftPointer();
        public ILaserPointer GetRightPointer();
        public GameObject GetLeftHand();
        public GameObject GetRightHand();
        public Camera GetCamera();
        public XRType XRType { get; }
        public string[] GetControllerActions();
        public event ActionChangedHandler OnActionChanged;
    }
}