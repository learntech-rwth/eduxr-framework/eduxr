﻿using UnityEngine;

namespace OmiLAXR
{
    public class Guide : MonoBehaviour
    {
        private static Guide _instance;

        public static Guide Instance
            => _instance ??= FindObjectOfType<Guide>();
    }
}