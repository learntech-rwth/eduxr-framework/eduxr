﻿using System.Collections.Generic;
using UnityEngine;

namespace OmiLAXR
{

    /// <summary>
    /// Script that syncs active states of objects to a tracked object.
    /// </summary>

    public class SyncActiveStates : MonoBehaviour
    {
        [SerializeField, Tooltip("Reference to object which should be tracked for state")]
        private GameObject trackedObject;

        [Tooltip("Objects, which should follow the active state of the tracked object.")]
        public List<GameObject> onEnabledObjects;

        [Tooltip("Objects, which should have the opposite active state of the tracked object.")]
        public List<GameObject> onDisabledObjects;

        private bool _currentState;

        private void Start()
        {
            SetVRState(trackedObject.activeInHierarchy);
        }

        private void Update()
        {
            // set all tracked objects to an active state depending on whether vr mode has currently been toggled on/off
            if (_currentState != trackedObject.activeInHierarchy) SetVRState(!_currentState);
        }

        private void SetVRState(bool isVr)
        {
            _currentState = isVr;
            foreach (var obj in onEnabledObjects) obj.SetActive(_currentState);
            foreach (var obj in onDisabledObjects) obj.SetActive(!_currentState);
        }
    }
}