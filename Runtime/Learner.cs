﻿using System;
using UnityEngine;
using OmiLAXR.Adapters;
using OmiLAXR.Interaction;

namespace OmiLAXR
{
    /// <summary>
    /// This Component is for applying user-centered design. Everything relevant for Learner behaviours is placed here.
    /// </summary>
    [DisallowMultipleComponent]
    [DefaultExecutionOrder(-1)]
    public class Learner : SingletonBehaviour
    {
        public static class Body
        {
            public readonly struct Hand
            {
                public readonly Side Side;
                public readonly GameObject GameObject;

                public Hand(Side side, GameObject gameObject)
                {
                    Side = side;
                    GameObject = gameObject;
                }

                public override string ToString() => Side.ToString();
            }
            /// <summary>
            /// Can be used to differ between Primary and Secondary
            /// </summary>
            public enum EquipmentType
            {
                Unknown,
                Primary,
                Secondary
            }
            /// <summary>
            /// Can be used to differe between Left and Right
            /// </summary>
            public enum Side
            {
                Unknown,
                Left,
                Right,
            }
            /// <summary>
            /// Can be used to describe learners actual pose.
            /// </summary>
            public enum Pose
            {
                Stands,
                Sits,
                Runs,
                Crouches
            }
        }
        
        /// <summary>
        /// Reference to the game object representing left hand of the learner.
        /// </summary>
        [HideInInspector] public GameObject leftHand;
        /// <summary>
        /// Reference to the game object representing left hand of the learner.
        /// </summary>
        [HideInInspector] public GameObject rightHand;

        [HideInInspector] public Body.Side primaryHandSide = Body.Side.Left;

        private Body.Side SecondaryHandSide => primaryHandSide == Body.Side.Left ? Body.Side.Right : Body.Side.Left;
        
        private static Learner _instance;
        /// <summary>
        /// Singleton instance of the Learner. Only one can exist at a time.
        /// </summary>
        public static Learner Instance
        {
            get
            {
                if ( _instance == null )
                {
                    _instance = FindObjectOfType<Learner>();
                    if (_instance == null)
                    {
                        // if is still null
                        DebugLog.OmiLAXR.Error("Cannot find Learner instance. Make sure you have added a 'Learner' component.");
                    }
                }
                return _instance;
            }
        }
        
        public override MonoBehaviour GetInstance() => Instance;

        public XRType XRType => Player?.XRType ?? XRType.Unknown;
        
        /// <summary>
        /// Method which states the VR mode.
        /// </summary>
        /// <returns>True, if learner is using VR.</returns>
        public bool IsVR => XRType == XRType.VirtualReality;
        public bool IsAR => XRType == XRType.AugmentedReality;
        public bool IsAV => XRType == XRType.AugmentedVirtuality;
        public bool IsMR => XRType == XRType.MixedReality;
        public bool IsCaveVR => XRType == XRType.CaveVR;
        public bool IsDesktop => XRType == XRType.Desktop;
        public bool IsXR => IsVR || IsCaveVR || IsAR || IsAR || IsMR || IsAV;

        public bool SupportsEyeTracking => IsXR; // todo: better recognition
        
        public event OnPlayerLoadHandler OnLoadPlayer;

        private IPlayer _player;

        public IPlayer Player
        {
            get
            {
                if (_player == null)
                    DebugLog.OmiLAXR.Error("Cannot find Player instance. Make sure you have used a correct adapter for your XR system.");
                return _player;
            }
            private set => _player = value;
        }

        public bool IsPlayerLoaded { get; private set; }

        public void AssignPlayer(IPlayer player)
        {
            if (player == null)
                return;

            Player = player;
            OnLoadPlayer?.Invoke(player);
            IsPlayerLoaded = true;

            leftHand = Player.GetLeftHand();
            rightHand = Player.GetRightHand();
        }

        /// <summary>
        /// Get LaserPointer according to passed side (Left or Right)
        /// </summary>
        /// <param name="side">.Left or .Right</param>
        /// <returns>Left or Right Laser Pointer reference.</returns>
        public ILaserPointer GetLaserPointer(Body.Side side) => side == Body.Side.Left ? Player.GetLeftPointer() : Player.GetRightPointer();

        /// <summary>
        /// Get primary or secondary LaserPointer.
        /// </summary>
        /// <param name="type">.Primary or .Secondary</param>
        /// <returns>Primary or Secondary Laser Pointer reference.</returns>
        public ILaserPointer GetLaserPointer(Body.EquipmentType type) =>
            GetLaserPointer(type == Body.EquipmentType.Primary ? primaryHandSide : SecondaryHandSide);
        public Body.Hand GetHand(Body.Side side) => new Body.Hand(side, Body.Side.Left == side ? leftHand : rightHand);
        public Body.Hand GetPrimaryHand() => GetHand(primaryHandSide);
        public Body.Hand GetSecondaryHand() => GetHand(SecondaryHandSide);
        public ILaserPointer GetPrimaryLaserPointer() => GetLaserPointer(primaryHandSide);

        public Body.Side RecognizeSide(GameObject handGameObject)
        {
            var left = Player.GetLeftPointer().GetGameObject();
            var right = Player.GetRightPointer().GetGameObject();

            if (handGameObject == left)
                return Body.Side.Left;
            
            return handGameObject == right ? Body.Side.Right : Body.Side.Unknown;
        }
        public Body.Side SetPrimaryHand(ILaserPointer laserPointer) => SetPrimaryHand(laserPointer.GetGameObject());
        public Body.Side SetPrimaryHand(GameObject handGameObject)
        {
            // Recognize side
            var side = RecognizeSide(handGameObject);
            // Set primary hand
            primaryHandSide = side;
            return side;
        }

        private void Start()
        {
            DontDestroyOnLoad(this);
        }
    }
}