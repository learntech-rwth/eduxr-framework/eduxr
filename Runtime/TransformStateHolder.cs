﻿using UnityEngine;

namespace OmiLAXR
{
    public class TransformStateHolder
    {
        public struct TransformStateHolderState
        {
            public readonly Vector3 Position;
            public readonly Vector3 LocalScale;
            public readonly Quaternion Rotation;

            public TransformStateHolderState(Vector3 position, Vector3 localScale, Quaternion rotation)
            {
                Position = position;
                LocalScale = localScale;
                Rotation = rotation;
            }
        }
        
        public TransformStateHolderState SavedState { get; private set; }
        public TransformStateHolderState CurrentState => new TransformStateHolderState(Transform.position, Transform.localScale, Transform.rotation);
        
        public Transform Transform { get; private set; }

        public TransformStateHolder(Transform transform)
        {
            Transform = transform;
        }

        public void SaveState()
            => SavedState = CurrentState;

        public void RestoreState()
        {
            Transform.position = SavedState.Position;
            Transform.rotation = SavedState.Rotation;
            Transform.localScale = SavedState.LocalScale;
        }
    }
}