﻿using UnityEngine;

namespace OmiLAXR.Extensions
{
    public static class Mesh_Ext
    {
        /// <summary>
        /// Create triangle from three vectors and their colors.
        /// </summary>
        public static Mesh CreateTriangle(Vector3[] points, Color[] colors = null)
            => CreateFrom(points, new int[] { 0, 1, 2 }, colors);
        /// <summary>
        /// Create a mesh based on the provided points, triangles and colors.
        /// </summary>
        public static Mesh CreateFrom(Vector3[] points, int[] triangles, Color[] colors = null)
        {
            var j = 0;
            var vertices = new Vector3[triangles.Length];
            var normals = new Vector3[triangles.Length];

            for (var i = 0; i < triangles.Length / 3; i++)
            {
                // get vertices
                var a = points[triangles[i * 3 + 0]];
                var b = points[triangles[i * 3 + 1]];
                var c = points[triangles[i * 3 + 2]];
                // Compute triangle normal
                var n = Vector3.Cross(b - a, c - a).normalized;
                // Assing normal to each vertex
                normals[i * 3 + 0] = n;
                normals[i * 3 + 1] = n;
                normals[i * 3 + 2] = n;
                vertices[i * 3 + 0] = a;
                vertices[i * 3 + 1] = b;
                vertices[i * 3 + 2] = c;
                triangles[i * 3 + 0] = j++;
                triangles[i * 3 + 1] = j++;
                triangles[i * 3 + 2] = j++;
            }
            // assign values to a Mesh instance
            var mesh = new Mesh();
            mesh.vertices = vertices;
            mesh.normals = normals;
            mesh.triangles = triangles;

            if (colors != null)
                mesh.colors = colors;

            return mesh;
        }
    }

}