﻿using UnityEngine.UI;

namespace OmiLAXR.xAPI.Extensions
{
    public static class Dropdown_Ext
    {
        public static void SetDisabled(this Dropdown dropdown, bool flag)
        {
            dropdown.OnDeselect(null);
            dropdown.interactable = !flag;
        }
        public static string GetTextOrDefault(this Dropdown dropdown, string defaultText = "")
        {
            var textMesh = dropdown.GetComponentInChildren<Text>();
            return !textMesh ? defaultText : textMesh.text;
        }
    }
}