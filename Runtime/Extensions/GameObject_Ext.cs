﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OmiLAXR.Extensions
{
    public static class GameObject_Ext
    {
        /// <summary>
        /// If exists, returns component of type T. Otherwise creates one and returns this.
        /// </summary>
        /// <typeparam name="T">Component type</typeparam>
        /// <param name="gameObject">applied gameobject</param>
        /// <returns>Returned component reference</returns>
        public static T AddOrGetComponent<T>(this GameObject gameObject) where T : Component
            => gameObject.GetComponent<T>() ?? gameObject.AddComponent<T>();
        public static GameObject Find(this GameObject gameObject, string name, bool includeInactive = false) => gameObject.transform.FindGameObject(name, includeInactive);
        public static GameObject FindDeepChild(this GameObject gameObject, string name, bool includeInactive = false)
        {
            var t = gameObject.transform.FindDeepChild(name);
            return t == null ? null : t.gameObject;
        }
        /// <summary>
        /// Apply action on each child in gameObject.
        /// </summary>
        public static IEnumerable<GameObject> ForEachChild(this GameObject gameObject, Action<GameObject> action)
        {
            var res = new List<GameObject>();

            if (gameObject == null || gameObject.transform == null)
                return res;
            
            for (var i = 0; i < gameObject.transform.childCount; i++)
            {
                var child = gameObject.transform.GetChild(i);
                if (child == null)
                    continue;
                var go = child.gameObject;
                action(go);
                res.Add(go);
            }
            return res;
        }
        
    }
}
