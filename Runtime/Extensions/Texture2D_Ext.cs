﻿using UnityEngine;

namespace OmiLAXR.Extensions
{
    public static class Texture2D_Ext 
    {
        /// <summary>
        /// Picks a color from palette by (x,y) vector.
        /// </summary>
        /// <param name="v">(x,y) texture float position of palette between 0.0f and 1.0f.</param>
        /// <returns>Color from palette</returns>
        public static Color PickColor(this Texture2D texture, Vector2 v) => PickColor(texture, v.x, v.y);
        /// <summary>
        /// Picks a color from palette by (x,y) numbers.
        /// </summary>
        /// <param name="x">x pixel coordinate of palette between 0 and width.</param>
        /// <param name="y">y pixel coordinate of palette between 0 and height.</param>
        /// <returns>Color from palette</returns>
        public static Color PickColor(this Texture2D texture, int x, int y) => texture.GetPixel(x, y);
        /// <summary>
        /// Picks a color from palette by (x,y) floats.
        /// </summary>
        /// <param name="x">x texture coordinate of palette between 0.0f and 1.0f.</param>
        /// <param name="y">y texture coordinate of palette between 0.0f and 1.0f.</param>
        /// <returns>Color from palette</returns>
        public static Color PickColor(this Texture2D texture, float x, float y)
        {
            // Get center of texture in form of pixels.
            var halfX = texture.width / 2;
            var halfY = texture.height / 2;
            // Get target position outgoing from center.
            var tx = halfX + x * halfX;
            var ty = halfY + y * halfY;
            return PickColor(texture, (int)tx, (int)ty);
        }
    }

}