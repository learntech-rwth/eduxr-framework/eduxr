﻿using TMPro;
using UnityEngine.UI;

namespace OmiLAXR.xAPI.Extensions
{
    public static class Slider_Ext
    {
        public static void SetDisabled(this Slider slider, bool flag)
        {
            slider.OnDeselect(null);
            slider.interactable = !flag;
        }
        public static string GetTextOrDefault(this Slider slider, string defaultText = "")
        {
            var textMesh = slider.GetComponentInChildren<TextMeshProUGUI>();
            return !textMesh ? defaultText : textMesh.text;
        }
    }
}