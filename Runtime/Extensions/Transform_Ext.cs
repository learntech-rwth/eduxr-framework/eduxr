﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OmiLAXR.Extensions
{
    public static class Transform_Ext
    {

        public static void ResetLocal(this Transform transform)
        {
            transform.localPosition = Vector3.zero;
            transform.eulerAngles = Vector3.zero;
            transform.localScale = Vector3.one;
        }
        public static GameObject FindGameObject(this Transform transform, string name, bool includeInactive = false)
        {
            var t = transform.GetComponentsInChildren<Transform>(includeInactive).FirstOrDefault(t => t.name == name);
            return t == null ? null : t.gameObject;
        }

        public static IEnumerable<GameObject> FindGameObjects(this Transform transform, params string[] names)
            => (from name in names select transform.Find(name) into t where t select t.gameObject);

        public static Transform FindDeepChild(this Transform origin, string name, bool includeInactive = false)
        {
            if (includeInactive)
            {
                // beta version
                var allChildren = origin.GetComponentsInChildren<Transform>(true);
                return allChildren.FirstOrDefault(c => c.name == name);
            }
            
            // copied from https://answers.unity.com/questions/799429/transformfindstring-no-longer-finds-grandchild.html
            var queue = new Queue<Transform>();
            queue.Enqueue(origin);
            
            while (queue.Count > 0)
            {
                var c = queue.Dequeue();
                if (c.name == name)
                    return c;
                
                foreach (Transform t in c)
                    queue.Enqueue(t);

                /* Dangerous
                var children = c.GetComponentsInChildren(typeof(Transform), includeInactive)
                    .Select(comp => (Transform)comp);
               
                foreach (var t in children)
                    queue.Enqueue(t); */
            }
            return null;
        }
        
        public static void DestroyChildren(this Transform transform)
        {
            for (var i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).Destroy();
            }
        }

        public static IEnumerable<Transform> ForEachChild(this Transform transform, Action<Transform> action)
        {
            var res = new List<Transform>();
            for (var i = 0; i < transform.childCount; i++)
            {
                var t = transform.GetChild(i);
                action(t);
                res.Add(t);
            }
            return res;
        }
    }

}