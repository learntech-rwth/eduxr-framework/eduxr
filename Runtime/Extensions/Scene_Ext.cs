﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace OmiLAXR.Extensions
{
    public static class Scene_Ext
    {
        public static void DestroyChildsImmediate<T>(this Scene scene) where T : Component
        {
            var roots = scene.GetRootGameObjects();
            foreach (var go in roots)
            {
                if (go.GetComponent<T>())
                {
                    Object.DestroyImmediate(go);
                    continue;
                }
                var childs = go.GetComponentsInChildren<T>();
                foreach (var c in childs)
                    Object.DestroyImmediate(c.gameObject);
            }
        }

        public static void DestroyChildsImmediate(this Scene scene, string gameObjectName)
        {
            var roots = scene.GetRootGameObjects();
            foreach (var root in roots)
            {
                if (root.name == gameObjectName)
                {
                    Object.DestroyImmediate(root);
                    continue;
                }

                var child = root.FindDeepChild(gameObjectName);
                Object.DestroyImmediate(child);
            }
        }
    }
}