﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace OmiLAXR.Extensions
{
    public static class Enum_Ext
    {
        /// <summary>
        /// Transforms Enum values to IEnumerable.
        /// </summary>
        public static IEnumerable<T> GetValues<T>(this T value) where T : Enum
            => Enum.GetValues(typeof(T)).Cast<T>().Where(e => value.HasFlag(e));

        /// <summary>
        /// Transforms Enum values to an array.
        /// </summary>
        public static T[] ToArray<T>(this T value) where T : Enum
            => value.GetValues().ToArray();
        /// <summary>
        /// Transforms Enum values to a list.
        /// </summary>
        public static List<T> ToList<T>(this T value) where T : Enum
            => value.GetValues().ToList();
        /// <summary>
        /// Checks if an flag is exactly in an array. Try to avoid this function in realtime, because of O(n²) performance.
        /// </summary>
        public static bool IsIn<T>(this T value, T[] array) where T : Enum
            => array.Contains(value);

        /// <summary>
        /// Checks if one flag is in an array. Try to avoid this function in realtime, because of O(n²) performance.
        /// </summary>
        public static bool OneIn<T>(this T value, T[] array) where T : Enum
        {
            var values = ToArray(value);
            foreach (var v in values)
                if (array.Contains(v))
                    return true;
            return false;
        }
        /// <summary>
        /// Checks if all flags are in an array. Try to avoid this function in realtime, because of O(n²) performance.
        /// </summary>
        public static bool AllIn<T>(this T value, T[] array) where T : Enum
        {
            var values = ToArray(value);
            foreach (var v in values)
                if (!array.Contains(v))
                    return false;
            return false;
        }
    }

}
