﻿using UnityEngine;
using System.Collections.Generic;

namespace OmiLAXR.Extensions
{
    public static class MonoBehaviour_Ext
    {
        public static void Log(this MonoBehaviour mono, string sender, string message)
        {
            DebugLog.OmiLAXR.Print($"<b>[{sender}]</b> {message}");
        }
        public static void DestroyChildren(this MonoBehaviour gameObject)
        {
            foreach (Transform child in gameObject.transform)
            {
                Object.Destroy(child.gameObject);
            }
        }
        public static void Destroy(this MonoBehaviour _, Object[] list)
        {
            foreach (var t in list)
            {
                Object.Destroy(t);
            }
        }

        public static void Destroy(this MonoBehaviour _, IEnumerable<Object> list)
        {
            foreach (var t in list)
            {
                Object.Destroy(t);
            }
        }
    }

}
