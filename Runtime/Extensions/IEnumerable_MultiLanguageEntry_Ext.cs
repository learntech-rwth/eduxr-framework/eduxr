﻿using System.Collections.Generic;
using System.Linq;
using OmiLAXR.Timeline.MultiLanguage;
using UnityEngine;

namespace OmiLAXR.Extensions
{
    public static class IEnumerable_MultiLanguageEntry_Ext
    {
        /// <summary>
        /// Get text of Multilanguage
        /// </summary>
        public static string GetText(this IEnumerable<MultiLanguageEntry> entries, MultiLanguageLanguages language,
            bool isDesktopVariant = false)
        {
            var multiLanguageEntries = entries.ToList();
            if (!multiLanguageEntries.Any())
                return "";
            var entry = multiLanguageEntries.SingleOrDefault(t => t.language == language);
            if (isDesktopVariant && !string.IsNullOrEmpty(entry.desktopVariantText))
            {
                return entry.desktopVariantText;
            }
            return string.IsNullOrEmpty(entry.text) ? multiLanguageEntries.FirstOrDefault().text : entry.text;
        }
        
        /// <summary>
        /// Get audio of Multilanguage
        /// </summary>
        public static AudioClip GetAudio(this IEnumerable<MultiLanguageEntry> entries, MultiLanguageLanguages language,
            bool isDesktopVariant = false)
        {
            var multiLanguageEntries = entries.ToList();
            if (!multiLanguageEntries.Any())
                return null;
            var entry = multiLanguageEntries.SingleOrDefault(t => t.language == language);
            if (isDesktopVariant && !string.IsNullOrEmpty(entry.desktopVariantText))
            {
                return entry.desktopAudio;
            }
            return entry.textAudio == null ? multiLanguageEntries.FirstOrDefault().textAudio : entry.textAudio;
        }
    }
}
