﻿using UnityEngine;

namespace OmiLAXR.Extensions
{
    public static class Object_Ext
    {
        /// <summary>
        /// This function can be used to destroy objects safety while editor or playing mode. 
        /// It uses Object.Destroy(obj) while playing and Object.
        /// And uses Object.DestroyImmediate(obj) in Editor mode.
        /// </summary>
        /// <param name="obj">Object you want to destroy.</param>
        public static void Destroy(this Object obj)
        {
            if (Application.isPlaying)
                Object.Destroy(obj);
            else
                Object.DestroyImmediate(obj);
        }
    }
}
