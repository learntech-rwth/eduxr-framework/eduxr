﻿using UnityEngine;

namespace OmiLAXR.Register
{
    public abstract class RegisterObject: MonoBehaviour
    {
        public abstract string GetObjectName();
        protected abstract void RegisterAtSystem();
        private void Reset() => RegisterAtSystem();
    }
}