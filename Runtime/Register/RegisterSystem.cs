﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace OmiLAXR.Register
{
    public abstract class RegisterSystem<T> : MonoBehaviour where T : RegisterObject
    {
        [HideInInspector] public List<T> registeredObjects = new List<T>();
        [HideInInspector] public bool orderByName = true;

        public void RegisterAllAvailable()
        {
            registeredObjects.Clear();
            foreach (var obj in FindObjectsOfType<T>())
                RegisterAtSystem(obj);
        }

        public bool RegisterAtSystem(T obj)
        {
            if (registeredObjects.Exists(regObj => regObj == obj))
                return false;
            registeredObjects.Add(obj);
            if (orderByName)
                OrderObjectsByName();
            return true;
        }

        public void OrderObjectsByName() =>
            registeredObjects = registeredObjects != null ? 
                new List<T>(registeredObjects!
                    .Where(o => o)
                    .OrderBy(regObj => regObj.GetObjectName())) 
                : new List<T>();

        public void RemoveMissingReferences() =>
            registeredObjects = registeredObjects.Where(obj => obj).ToList();

        private void Reset()
        {
            if (FindObjectsOfType<RegisterSystem<T>>(true).Length > 1)
            {
                DebugLog.OmiLAXR.Warning($"There is already a register system for {typeof(T).Name} present in this scene!");
                DestroyImmediate(this);
            }

            RegisterAllAvailable();
        }
    }
}