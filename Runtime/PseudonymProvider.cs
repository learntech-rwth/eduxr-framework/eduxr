﻿using System;
using OmiLAXR.Helpers;
using UnityEngine;

namespace OmiLAXR
{
    public class PseudonymProvider : MonoBehaviour
    {
        private struct PseudonymResponse
        {
            public string pseudonym;
        }

        public string baseUrl = "https://mtlg.elearn.rwth-aachen.de/pseudo/pseudonym/";
        public string languageCode = "en";
        public bool capitalize = true;
        public bool rememberName = true;

        public event Action<string> OnSetPseudonym;

        public string url => UriHelper.CombineToString(baseUrl, languageCode);

        public void GeneratePseudonym(Action<string> callback = null)
        {
            var rememberedName = LoadPseudonym();
            if (rememberName && rememberedName != null)
            {
                OnSetPseudonym?.Invoke(rememberedName);
                callback?.Invoke(rememberedName);
                return;
            }

            // Create URI according to https://mtlg.elearn.rwth-aachen.de/pseudo/
            var qb = new QueryBuilder(url);
            qb.Append("capitalize", capitalize);

            var uri = qb.ToString();

            StartCoroutine(WebRequestHelper.GetJsonRequest<PseudonymResponse>(uri, (code, pr) => {
                OnSetPseudonym?.Invoke(pr.pseudonym);
                callback?.Invoke(pr.pseudonym);

                if (rememberName)
                    SavePseudonym(pr.pseudonym);
            }));
        }


        string LoadPseudonym()
        {
            return PlayerPrefs.HasKey("pseudonym") ? PlayerPrefs.GetString("pseudonym") : null;
        }

        void SavePseudonym(string pseudonym)
        {
            PlayerPrefs.SetString("pseudonym", pseudonym);
            PlayerPrefs.Save();
        }
    }
}