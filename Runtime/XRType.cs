﻿namespace OmiLAXR
{
    public enum XRType
    {
        Unknown,
        Desktop,
        AugmentedReality,
        AugmentedVirtuality,
        MixedReality,
        VirtualReality,
        CaveVR
    }
}