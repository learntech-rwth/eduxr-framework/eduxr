﻿using UnityEngine;

using OmiLAXR.Extensions;

namespace OmiLAXR
{
    public class ColorPicker : MonoBehaviour
    {
        [Tooltip("Color palette texture on which the color picking should be applied.")]
        public Sprite gradientSprite;

        /// <summary>
        /// GameObject which represents the color picker palette in 3D space.
        /// </summary>
        public GameObject gradientObject { get; private set; }

        private void Awake()
        {
            CreateGradientObject();
        }

        /// <summary>
        /// Initialization function for gradient's game object.
        /// </summary>
        private void CreateGradientObject()
        {
            // Create gradient texture quad
            gradientObject = new GameObject("GradientObject");
            gradientObject.transform.SetParent(transform);

            var sr = gradientObject.AddComponent<SpriteRenderer>();
            sr.sprite = gradientSprite;
        }

        /// <summary>
        /// Picks a color from palette by (x,y) vector.
        /// </summary>
        /// <param name="v">(x,y) texture float position of palette between 0.0f and 1.0f.</param>
        /// <returns>Color from palette</returns>
        public Color PickColor(Vector2 v) => PickColor(v.x, v.y);

        /// <summary>
        /// Picks a color from palette by (x,y) floats.
        /// </summary>
        /// <param name="x">x texture coordinate of palette between 0.0f and 1.0f.</param>
        /// <param name="y">y texture coordinate of palette between 0.0f and 1.0f.</param>
        /// <returns>Color from palette</returns>
        public Color PickColor(float x, float y) => gradientSprite.texture.PickColor(x, y);
    }

}