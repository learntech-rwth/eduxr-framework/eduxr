﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;


namespace OmiLAXR.Register
{
    public abstract class RegisterSystemInspector<T> : Editor where T : RegisterObject
    {
        private RegisterSystem<T> _registerSystem;
        private ReorderableList _registeredObjectList;
        private const float Aspect = 1f / 2;
        private const float Padding = 25f;
        private List<bool> _flags = new List<bool>();

        private SerializedProperty _orderByNameProperty;

        private void OnEnable()
        {
            if (target is null)
                return;

            _registerSystem = (RegisterSystem<T>) target;

            _orderByNameProperty = serializedObject.FindProperty("orderByName");

            _registeredObjectList = new ReorderableList(
                serializedObject: serializedObject,
                elements: serializedObject.FindProperty("registeredObjects"),
                draggable: !_orderByNameProperty.boolValue,
                displayHeader: true,
                displayAddButton: false,
                displayRemoveButton: false
            );
            _flags = new List<bool>();
            _registeredObjectList.drawHeaderCallback = ListHeader;
            _registeredObjectList.drawElementCallback = ListElement;
        }

        public override void OnInspectorGUI()
        {
            if (_orderByNameProperty == null)
            {
                _orderByNameProperty = serializedObject.FindProperty("orderByName");
            }
            
            serializedObject.Update();
            DrawDefaultInspector();
            
            if (_flags.Count > 0)
                EditorGUILayout.Separator();
            if (_flags.Count == 1)
                EditorGUILayout.HelpBox($"There is {_flags.Count} missing reference in the registered hints. Please refresh.", MessageType.Info);
            if (_flags.Count > 1)
                EditorGUILayout.HelpBox($"There are {_flags.Count} missing references in the registered hints. Please refresh.", MessageType.Info);
            _flags.Clear();
            
            EditorGUILayout.Separator();
            
            var newOrderByName = EditorGUILayout.Toggle("Order By Name", _orderByNameProperty.boolValue);
            if (newOrderByName != _orderByNameProperty.boolValue && newOrderByName)
            {
                _registerSystem.OrderObjectsByName();
                serializedObject.Update();
            }
            _orderByNameProperty.boolValue = newOrderByName;
            
            _registeredObjectList.draggable = !_orderByNameProperty.boolValue;
            _registeredObjectList.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }

        private void ListHeader(Rect rect)
        {
            var registeredObjects = new List<T>();
            var nullRefFound = false;
            foreach (var regObj in _registerSystem.registeredObjects)
            {
                if (!regObj)
                {
                    nullRefFound = true;
                    continue;
                }
                registeredObjects.Add(regObj);
            }
            
            if(nullRefFound)
                _registerSystem.RemoveMissingReferences();

            var numberAll = registeredObjects.Count;
            var active = registeredObjects.FindAll(regObj => regObj.enabled);
            var numberActive = active.Count;

            var groupedActive = active.GroupBy(regObj => regObj.GetObjectName());
            var group = registeredObjects.GroupBy(regObj => regObj.GetObjectName());

            var buttonWidth = Mathf.Min(rect.width / 2, 60f);
            var buttonRect = new Rect(rect.xMax - buttonWidth, rect.y + 1, buttonWidth, rect.height - 2);
            var labelRect = new Rect(rect.x, rect.y, rect.width - buttonWidth, rect.height);
            var labelText = $"Registered {typeof(T).Name}s (Active/Total) ({numberActive}/{numberAll}," +
                            $" where unique names: {numberActive}/{numberAll})";
            EditorGUI.LabelField(labelRect, labelText);
            if (GUI.Button(buttonRect, "Refresh"))
            {
                _flags.Clear();
                _registerSystem.RegisterAllAvailable();
            }
        }

        private void ListElement(Rect rect, int index, bool active, bool focused)
        {
            var element = _registeredObjectList.serializedProperty.GetArrayElementAtIndex(index);
            if (element?.objectReferenceValue is null)
            {
                _flags.Add(false);
                return;
            }
            
            var registeredObject = element.objectReferenceValue as RegisterObject;
            if (!registeredObject)
            {
                _flags.Add(false);
                return;
            }

            // Better vertical center
            rect.y += 2;

            var toggleRect = new Rect(rect.x, rect.y, Padding, EditorGUIUtility.singleLineHeight);
            registeredObject.enabled = EditorGUI.Toggle(toggleRect, registeredObject.enabled);

            // Object name
            var labelFieldRect = new Rect(rect.x + Padding, rect.y, rect.width * Aspect - Padding,
                EditorGUIUtility.singleLineHeight);
            EditorGUI.LabelField(labelFieldRect, registeredObject.GetObjectName());

            // Adds a object field to navigate quickly to a trackable
            var objectFieldRect = new Rect(rect.x + Padding / 2 + rect.width * Aspect, rect.y,
                rect.width * (1 - Aspect) - Padding / 2, EditorGUIUtility.singleLineHeight);
            GUI.enabled = false;
            EditorGUI.ObjectField(objectFieldRect, element, GUIContent.none);
            GUI.enabled = true;
        }
    }
}