﻿# if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace OmiLAXR.Register
{
    public class RegisterObjectInspector<T> : Editor where T : RegisterObject
    {
        private RegisterSystem<T> _registerSystem;

        private void OnDisable() => FindObjectOfType<RegisterSystem<T>>()?.RemoveMissingReferences();

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawPropertyFields();
            serializedObject.ApplyModifiedProperties();
        }

        private void DrawPropertyFields()
        {
            var prop = serializedObject.GetIterator();
            prop.NextVisible(true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty(prop.name), true);
            DrawRegisterSystemField();
            while (prop.NextVisible(false))
                EditorGUILayout.PropertyField(serializedObject.FindProperty(prop.name), true);
        }

        private void DrawRegisterSystemField()
        {
            GUI.enabled = false;
            CheckRegisterSystem();
            if (_registerSystem)
                EditorGUILayout.ObjectField("Register System:", _registerSystem, typeof(RegisterSystem<T>), true);
            GUI.enabled = true;
        }

        private void CheckRegisterSystem()
        {
            if (!_registerSystem)
                _registerSystem = FindObjectOfType<RegisterSystem<T>>();
        }
    }
}

#endif