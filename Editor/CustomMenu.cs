﻿using OmiLAXR.Utils;
using UnityEditor;

namespace OmiLAXR
{
    internal static class CustomMenu
    {
        [MenuItem("GameObject / OmiLAXR / Learner (mandatory)")]
        private static void AddLearner()
            => EditorUtils.AddPrefabToSelection("Prefabs/Learner", true);

        [MenuItem("GameObject / OmiLAXR / Interaction / Enable Laser Pointer Support")]
        private static void EnableLaserPointerInteraction()
            => EditorUtils.AddPrefabTo<Learner>("Prefabs/Interaction/Laser Pointer Handler", true);
        
        [MenuItem("GameObject / OmiLAXR / Interaction / Enable Mouse Interaction Support")]
        private static void EnableMouseInteraction()
            => EditorUtils.AddPrefabTo<Learner>("Prefabs/Interaction/Mouse Interaction Handler", true);

        [MenuItem("GameObject / OmiLAXR / Learning Environment")]
        private static void AddLearningEnvironment()
            => EditorUtils.AddPrefabToSelection("Prefabs/Learning Environment", true);
    }
}
