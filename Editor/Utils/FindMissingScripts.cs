﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace OmiLAXR.Utils
{
    internal static class FindMissingScripts
    {
        [MenuItem("OmiLAXR / ::Debug:: / Find Missing Scripts / in Project")]
        [MenuItem("GameObject / OmiLAXR / ::Debug:: / Find Missing Scripts / in Project")]
        private static void FindMissingScriptsInProject()
        {
            var prefabPaths = AssetDatabase.GetAllAssetPaths()
                .Where(path => path.EndsWith(".prefab", System.StringComparison.OrdinalIgnoreCase)).ToArray();
            var found = 0;
            foreach (var path in prefabPaths)
            {
                var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                if (prefab.GetComponentsInChildren<Component>().All(comp => comp != null)) 
                    continue;
                Debug.LogError("Prefab found with missing script " + path, prefab);
                found++;
            }
            if (found == 0)
                Debug.Log("Cannot find any missing scripts in Project.");
        }

        [MenuItem("OmiLAXR / ::Debug:: / Find Missing Scripts / in Scene")]
        [MenuItem("GameObject / OmiLAXR / ::Debug:: / Find Missing Scripts / in Scene")]
        private static void FindMissingScriptsInScene()
        {
            var found = 0;
            foreach (var go in Object.FindObjectsOfType<GameObject>(true))
            {
                if (go.GetComponentsInChildren<Component>().All(comp => comp != null)) 
                    continue;
                Debug.LogError("GameObject found with missing script " + go.name, go);
                found++;
            }
            if (found == 0)
                Debug.Log("Cannot find any missing scripts in Scene.");
        }
    }
}